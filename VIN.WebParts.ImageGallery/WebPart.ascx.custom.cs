// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace VIN.WebParts.ImageGallery
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Globalization;
    using System.IO;
    using System.Linq;
    using System.Threading;
    using System.Web;

    using CMS.DocumentEngine;
    using CMS.EventLog;
    using CMS.Helpers;
    using CMS.MediaLibrary;
    using CMS.SiteProvider;

    using Deepend.Framework.Kentico;
    using Deepend.Framework.Kentico.Modules.ImageCrop;
    using Deepend.Framework.Kentico.Modules.ImageCrop.Models;

    using ImageProcessor;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Methods"

        /// <summary>
        /// Gets the items.
        /// </summary>
        public override List<ImageGalleryModel> Items {
            get
            {
                return base.Items.ToList();
            }
        }

        /// <summary>
        /// Gets the gallery title.
        /// </summary>
        public string GalleryTitle
        {
            get
            {
                var title = DataHelper.GetNotEmpty(this.GetValue("GalleryTitle"), string.Empty);

                return (!string.IsNullOrEmpty(title)) ? HttpUtility.UrlDecode(title) : string.Empty;
            }
        }

        #endregion
    }
}