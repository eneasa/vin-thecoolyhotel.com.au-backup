// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.ContentBlock
{
    using System;
    using System.Web.UI.HtmlControls;

    using CMS.PortalEngine;

    using Deepend.Framework.Kentico.Modules.ImageCrop;

    using TheCoolyHotel.Domain.Models;
    using CMS.DataEngine;
    using CMS.SiteProvider;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Properties"

        /// <summary>
        /// Gets the item.
        /// </summary>
        public override ContentBlockModel Item
        {
            get
            {
                try
                {
                    return base.Item;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        #endregion

        #region "Methods"

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);

                if (this.Item != null)
                {
                    var headMain = (HtmlHead)this.Page.Master.FindControl("mainHead");

                    if (!string.IsNullOrWhiteSpace(this.Item.DocumentName))
                    {
                        var htMetaTitle = new HtmlMeta();
                        htMetaTitle.Attributes.Add("name", "og:title");
                        htMetaTitle.Attributes.Add(
                            "content",
                            this.Item.DocumentName.Trim() + " - The Coolangatta Hotel");
                        headMain.Controls.Add(htMetaTitle);
                    }

                    if (!string.IsNullOrWhiteSpace(this.Item.Subtitle))
                    {
                        var htMetaDesc = new HtmlMeta();
                        htMetaDesc.Attributes.Add("name", "og:description");
                        htMetaDesc.Attributes.Add("content", this.Item.Subtitle);
                        headMain.Controls.Add(htMetaDesc);
                    }

                    if (this.Item.ImageGuid != null && !this.Item.ImageGuid.Equals(Guid.Empty))
                    {
                        var htMetaImg = new HtmlMeta();
                        htMetaImg.Attributes.Add("name", "og:image");
                        htMetaImg.Attributes.Add("content", ImageCropUtils.GetImageUrl(this.Item.ImageGuid, "300x300"));
                        headMain.Controls.Add(htMetaImg);
                    }
                }
            }
            catch
            {
            }
        }

        #endregion

        #region "Constants"

        /// <summary>
        /// Gets a value indicating whether edit mode.
        /// </summary>
        public bool EditMode
        {
            get
            {
                return this.PageManager.ViewMode == ViewModeEnum.Edit;
            }
        }

        /// <summary>
        /// Gets the current relative url.
        /// </summary>
        public string CurrentRelativeUrl
        {
            get
            {
                return this.CurrentDocument.RelativeURL;
            }
        }


        public String VenueDetailsLink
        {
            get
            {
                return SettingsKeyInfoProvider.GetStringValue(SiteContext.CurrentSiteName + ".MT_VenueDetailsLink");
            }
        }

        public String ContactUsLink
        {
            get
            {
                return SettingsKeyInfoProvider.GetStringValue(SiteContext.CurrentSiteName + ".MT_ContactUsLink");
            }
        }

        public String CurrentURLa
        {
            get
            {
                return CurrentDocument.NodeAlias;
            }
        }
        #endregion
    }
}