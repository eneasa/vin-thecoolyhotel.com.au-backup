// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.ContentBlock
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CMS.DataEngine;
    using CMS.DocumentEngine;
    using CMS.Helpers;

    using Deepend.Framework.Kentico.WebPart;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart, ContentBlockModel>
    {
        #region "WebPart Property Names"

        /// <summary>
        /// Gets the pro p_ shareable.
        /// </summary>
        public virtual string PROP_SHAREABLE
        {
            get
            {
                return "Shareable";
            }
        }

        #endregion

        #region "WebPart Default Values"

        /// <summary>
        /// Gets a value indicating whether defaul t_ shareable.
        /// </summary>
        public virtual bool DEFAULT_SHAREABLE
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region "WebPart Properties"

        /// <summary>
        /// Gets or sets a value indicating whether shareable.
        /// </summary>
        public bool Shareable
        {
            get
            {
                return ValidationHelper.GetBoolean(
                    this.GetValue(this.PROP_SHAREABLE), 
                    this.DEFAULT_SHAREABLE, 
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_SHAREABLE, value);
            }
        }

        /// <summary>
        /// Gets the quick links.
        /// </summary>
        public List<QuickLinkModel> QuickLinkUrls
        {
            get
            {
                var orderColumns = "NodeLevel, NodeOrder, NodeName";
                var quiklinksRootId = new Guid(this.GetValue("QuickLinks").ToString());

                var tp = new TreeProvider();
                var quickLinksItem = tp.SelectSingleNode(quiklinksRootId, this.CultureCode, this.CurrentSiteName);

                var quickLinks =
                        DocumentHelper.GetDocuments(QuickLinkModel.KENTICO_CLASSNAME)
                            .Path(quickLinksItem.NodeAliasPath, PathTypeEnum.Section)
                            .Published()
                            .Culture(this.CurrentDocument.DocumentCulture)
                            .OrderBy(OrderDirection.Ascending, orderColumns)
                            .TypedResult.Items.ToDomainModels<QuickLinkModel>()
                            .ToList();

                return quickLinks;
            }
        }

        #endregion
    }
}