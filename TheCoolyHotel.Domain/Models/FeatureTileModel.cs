﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FeatureTileModel.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The feature tile model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    /// <summary>
    /// The feature tile model.
    /// </summary>
    public partial class FeatureTileModel
    {
        /// <summary>
        /// The _ item tags string.
        /// </summary>
        private string _ItemTagsString;

        /// <summary>
        /// Gets the item tags string.
        /// </summary>
        public string ItemTagsString
        {
            get
            {
                if (this._ItemTagsString == null)
                {
                    var tags = this.ItemTags;

                    if (!string.IsNullOrEmpty(tags))
                    {
                        tags = tags.Replace("\"", string.Empty).ToLower();
                    }

                    this._ItemTagsString = tags;
                }

                return this._ItemTagsString;
            }
        }
    }
}