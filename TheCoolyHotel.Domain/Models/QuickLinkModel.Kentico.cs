﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="QuickLinkModel.Kentico.cs" company="">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The quick link.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The quick link.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class QuickLinkModel : InventoryModel, IInventoryModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.QuickLink";

        public string LinkText { get; set; }

        public string LinkUrl { get; set; }
    }
}
