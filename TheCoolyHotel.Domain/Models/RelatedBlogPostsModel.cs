﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RelatedBlogPostsModel.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The related blog posts model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    /// <summary>
    /// The related blog posts model.
    /// </summary>
    public partial class RelatedBlogPostsModel
    {
    }
}