﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BlogPostModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The blog post model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The blog post model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class BlogPostModel : InventoryModel, IInventoryModel, IImageModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "CMS.BlogPost";

        #region "DocType Properties"

        /// <summary>
        /// Gets or sets the blog post title.
        /// </summary>
        public string BlogPostTitle { get; set; }

        /// <summary>
        /// Gets or sets the blog post date.
        /// </summary>
        public DateTime BlogPostDate { get; set; }

        /// <summary>
        /// Gets or sets the blog post summary.
        /// </summary>
        public string BlogPostSummary { get; set; }

        /// <summary>
        /// Gets or sets the blog post body.
        /// </summary>
        public string BlogPostBody { get; set; }

        /// <summary>
        /// Gets or sets the blog post teaser.
        /// </summary>
        public string BlogPostTeaser { get; set; }

        /// <summary>
        /// Gets or sets the document tags.
        /// </summary>
        public string DocumentTags { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether featured.
        /// </summary>
        public bool Featured { get; set; }

        /// <summary>
        /// Gets or sets the image guid.
        /// </summary>
        public Guid? ImageGuid { get; set; }

        /// <summary>
        /// Gets or sets the image caption.
        /// </summary>
        public string ImageCaption { get; set; }

        #endregion
    }
}