﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AFeatureSlideModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   Defines the AFeatureSlideModel type.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The a feature slide model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class AFeatureSlideModel : InventoryModel, IInventoryModel, IImageModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.AFeatureSlide";

        #region "DocType Properties"

        /// <summary>
        /// Gets or sets the image guid.
        /// </summary>
        public Guid? ImageGuid { get; set; }

        /// <summary>
        /// Gets or sets the primary copy.
        /// </summary>
        public string PrimaryCopy { get; set; }

        /// <summary>
        /// Gets or sets the secondary copy.
        /// </summary>
        public string SecondaryCopy { get; set; }

        /// <summary>
        /// Gets or sets the read more text.
        /// </summary>
        public string ReadMoreText { get; set; }

        /// <summary>
        /// Gets or sets the read more url.
        /// </summary>
        public string ReadMoreUrl { get; set; }

        /// <summary>
        /// Gets or sets the video url.
        /// </summary>
        public string VideoUrl { get; set; }

        #endregion
    }
}