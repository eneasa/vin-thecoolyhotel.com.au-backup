﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SubnavLinkModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The subnav link model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The subnav link model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class SubnavLinkModel : InventoryModel, IInventoryModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.SubnavLink";

        #region "DocType Properties"

        /// <summary>
        /// Gets or sets the linked document.
        /// </summary>
        public Guid LinkedDocument { get; set; }

        /// <summary>
        /// Gets or sets the icon.
        /// </summary>
        public string Icon { get; set; }

        #endregion
    }
}