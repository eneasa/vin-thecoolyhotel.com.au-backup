﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StoreModel.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The store model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    /// <summary>
    /// The store model.
    /// </summary>
    public partial class StoreModel
    {
    }
}