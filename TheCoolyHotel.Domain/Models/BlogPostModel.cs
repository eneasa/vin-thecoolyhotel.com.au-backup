﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BlogPostModel.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The blog post model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    /// <summary>
    /// The blog post model.
    /// </summary>
    public partial class BlogPostModel
    {
    }
}