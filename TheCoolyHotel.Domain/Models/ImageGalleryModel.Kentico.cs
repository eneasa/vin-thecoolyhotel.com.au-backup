﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageGalleryModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The image gallery model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The image gallery model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class ImageGalleryModel : InventoryModel, IInventoryModel, IImageModel
    {
        /// <summary>
        /// The kentic document type.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.ImageGallery";

        /// <summary>
        /// Gets or sets the image GUID.
        /// </summary>
        public Guid? ImageGuid { get; set; }
    }
}
