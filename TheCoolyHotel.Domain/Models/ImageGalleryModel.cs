﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ImageGalleryModel.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The image gallery model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    /// <summary>
    /// The image gallery model.
    /// </summary>
    public partial class ImageGalleryModel
    {
    }
}
