﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContentBlockModel.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The content block model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    /// <summary>
    /// The content block model.
    /// </summary>
    public partial class ContentBlockModel
    {
    }
}