﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="FeatureTileModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The feature tile model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The feature tile model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class FeatureTileModel : InventoryModel, IInventoryModel, IImageModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.FeatureTile";

        #region "DocType Properties"

        /// <summary>
        /// Gets or sets the subtitle.
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// Gets or sets the body text.
        /// </summary>
        public string BodyText { get; set; }

        /// <summary>
        /// Gets or sets the category.
        /// </summary>
        public string Category { get; set; }

        /// <summary>
        /// Gets or sets the button text.
        /// </summary>
        public string ButtonText { get; set; }

        /// <summary>
        /// Gets or sets the button url.
        /// </summary>
        public string ButtonUrl { get; set; }

        /// <summary>
        /// Gets or sets the image guid.
        /// </summary>
        public Guid? ImageGuid { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether show image.
        /// </summary>
        public bool ShowImage { get; set; }

        /// <summary>
        /// Gets or sets the content date.
        /// </summary>
        public DateTime? ContentDate { get; set; }

        /// <summary>
        /// Gets or sets the item tags.
        /// </summary>
        public string ItemTags { get; set; }

        #endregion
    }
}