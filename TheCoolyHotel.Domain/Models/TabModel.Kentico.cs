﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="TabModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The tab model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The tab model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class TabModel : InventoryModel, IInventoryModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.Tab";

        #region "DocType Properties"

        /// <summary>
        /// Gets or sets the filter tag.
        /// </summary>
        public string FilterTag { get; set; }

        /// <summary>
        /// Gets or sets the image guid.
        /// </summary>
        public Guid? ImageGuid { get; set; }

        #endregion
    }
}