﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContentBlockModel.Kentico.cs" company="Deepend">
//   
// </copyright>
// <summary>
//   The content block model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The content block model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class ContentBlockModel : InventoryModel, IInventoryModel, IImageModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.ContentBlock";

        #region "DocType Properties"

        /// <summary>
        /// Gets or sets the subtitle.
        /// </summary>
        public string Subtitle { get; set; }

        /// <summary>
        /// Gets or sets the description.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets the image guid.
        /// </summary>
        public Guid? ImageGuid { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the open hours.
        /// </summary>
        public string OpenHours { get; set; }

        /// <summary>
        /// Gets or sets the location.
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Gets or sets the button text.
        /// </summary>
        public string ButtonText { get; set; }

        /// <summary>
        /// Gets or sets the button url.
        /// </summary>
        public string ButtonUrl { get; set; }

        /// <summary>
        /// Gets or sets the quick links.
        /// </summary>
        public Guid QuickLinks { get; set; }

        /// <summary>
        /// Gets or sets the link text.
        /// </summary>
        public string LinkText { get; set; }

        /// <summary>
        /// Gets or sets the link url.
        /// </summary>
        public string LinkUrl { get; set; }

        #endregion
    }
}