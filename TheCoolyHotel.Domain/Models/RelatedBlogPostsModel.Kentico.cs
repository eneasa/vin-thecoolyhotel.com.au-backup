﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="RelatedBlogPostsModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The related blog posts model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The related blog posts model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class RelatedBlogPostsModel : InventoryModel, IInventoryModel, IImageModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.RelatedBlogPosts";

        /// <summary>
        /// Gets or sets the image guid.
        /// </summary>
        public Guid? ImageGuid { get; set; }
    }
}