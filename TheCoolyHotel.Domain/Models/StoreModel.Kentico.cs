﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="StoreModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The store model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The store model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class StoreModel : InventoryModel, IInventoryModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.Store";

        #region "DocType Properties"

        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the map id.
        /// </summary>
        public int? MapId { get; set; }

        /// <summary>
        /// Gets or sets the phone number.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets the detail page.
        /// </summary>
        public string DetailPage { get; set; }

        /// <summary>
        /// Gets or sets the info text.
        /// </summary>
        public string InfoText { get; set; }

        /// <summary>
        /// Gets or sets the logo guid.
        /// </summary>
        public Guid? LogoGuid { get; set; }

        #endregion
    }
}