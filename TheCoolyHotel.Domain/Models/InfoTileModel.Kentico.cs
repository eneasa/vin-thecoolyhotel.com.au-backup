﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="InfoTileModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The info tile model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The info tile model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class InfoTileModel : InventoryModel, IInventoryModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "VIN.InfoTile";

        #region "DocType Properties"

        /// <summary>
        /// Gets or sets the map lat long.
        /// </summary>
        public string MapLatLong { get; set; }

        /// <summary>
        /// Gets or sets the contact address.
        /// </summary>
        public string ContactAddress { get; set; }

        /// <summary>
        /// Gets or sets the contact number.
        /// </summary>
        public string ContactNumber { get; set; }

        #endregion
    }
}