﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BlogMonthModel.Kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The blog month model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Domain.Models
{
    using System;

    using Deepend.Framework.Kentico.Attributes;
    using Deepend.Framework.Kentico.Interfaces;
    using Deepend.Framework.Kentico.Modules.Inventory.Models;

    /// <summary>
    /// The blog month model.
    /// </summary>
    [KenticoDocType(KENTICO_CLASSNAME)]
    public partial class BlogMonthModel : InventoryModel, IInventoryModel
    {
        /// <summary>
        /// The kentic o_ classname.
        /// </summary>
        public const string KENTICO_CLASSNAME = "CMS.BlogMonth";

        #region "DocType Properties"

        /// <summary>
        /// Gets or sets the blog month name.
        /// </summary>
        public string BlogMonthName { get; set; }

        /// <summary>
        /// Gets or sets the blog month starting date.
        /// </summary>
        public DateTime BlogMonthStartingDate { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether featured.
        /// </summary>
        public bool Featured { get; set; }

        /// <summary>
        /// Gets or sets the image guid.
        /// </summary>
        public Guid? ImageGuid { get; set; }

        #endregion
    }
}