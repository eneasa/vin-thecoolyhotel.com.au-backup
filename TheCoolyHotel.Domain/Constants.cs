﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Constants.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The constants.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System;
using System.Collections.Generic;

namespace TheCoolyHotel.Domain
{
    /// <summary>
    /// The constants.
    /// </summary>
    internal class Constants
    {
        /// <summary>
        /// The imagesiz e_ map.
        /// </summary>
        public const string IMAGESIZE_MAP = "Web:150x80";

        /// <summary>
        /// The imagesiz e_ website.
        /// </summary>
        public const string IMAGESIZE_WEBSITE = "Web:800x600";

        /// <summary>
        /// The imagesiz e_ gallery.
        /// </summary>
        public const string IMAGESIZE_GALLERY = "Web:800x600";

        /// <summary>
        /// The imagesiz e_ original.
        /// </summary>
        public const string IMAGESIZE_ORIGINAL = "Web:Originals";

        /// <summary>
        /// The imagesiz e_ thumbnail.
        /// </summary>
        public const string IMAGESIZE_THUMBNAIL = "API:150x100";

        /// <summary>
        /// The imagesiz e_ bethumbnail.
        /// </summary>
        public const string IMAGESIZE_BETHUMBNAIL = "API:125x125";

        /// <summary>
        /// The imagesiz e_ default.
        /// </summary>
        public const string IMAGESIZE_DEFAULT = "API:640x290";

        /// <summary>
        /// The imagesiz e_ icon.
        /// </summary>
        public const string IMAGESIZE_ICON = "Icon:44x44";

        /// <summary>
        /// The startofweek.
        /// </summary>
        public const DayOfWeek STARTOFWEEK = DayOfWeek.Monday;

        /// <summary>
        /// The image sizes.
        /// </summary>
        public static readonly Dictionary<string, string> ImageSizes = new Dictionary<string, string>
                                                                           {
                                                                               {
                                                                                   "Map", 
                                                                                   "Web:150x80"
                                                                               }, 
                                                                               {
                                                                                   "Website", 
                                                                                   "Web:800x600"
                                                                               }, 
                                                                               {
                                                                                   "Gallery", 
                                                                                   "Web:800x600"
                                                                               }, 
                                                                               {
                                                                                   "Original", 
                                                                                   "Web:Originals"
                                                                               }, 
                                                                               {
                                                                                   "Thumbnail", 
                                                                                   "API:150x100"
                                                                               }, 
                                                                               {
                                                                                   "BEThumbnail", 
                                                                                   "API:125x125"
                                                                               }, 
                                                                               {
                                                                                   "Default", 
                                                                                   "API:640x290"
                                                                               }, 
                                                                               {
                                                                                   "Icon", 
                                                                                   "Icon:44x44"
                                                                               }, 
                                                                               {
                                                                                   "HeroSmall", 
                                                                                   "Web:320x400"
                                                                               }, 
                                                                               {
                                                                                   "HeroMedium", 
                                                                                   "Web:768x520"
                                                                               }, 
                                                                               {
                                                                                   "HeroStandard", 
                                                                                   "Web:1024x437"
                                                                               }, 
                                                                               {
                                                                                   "HeroLarge", 
                                                                                   "Web:1920x570"
                                                                               }
                                                                           };
    }
}