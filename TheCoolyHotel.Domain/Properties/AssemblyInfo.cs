﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   AssemblyInfo.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System.Reflection;
using System.Runtime.InteropServices;

using CMS;

using Deepend.Framework.Attributes;

// Required for Kentico to pick up our assembly
[assembly: AssemblyDiscoverable]
[assembly: DeependAssembly]
[assembly: AssemblyTitle("TheCoolyHotel.Domain")]
[assembly: AssemblyDescription("TheCoolyHotel.Domain")]
[assembly: ComVisible(false)]
[assembly: Guid("3e1b86a5-2913-4c51-99c4-1d22297a96d7")]