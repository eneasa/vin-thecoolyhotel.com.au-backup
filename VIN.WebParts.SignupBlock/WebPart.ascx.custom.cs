// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.SignupBlock
{
    using CMS.PortalEngine;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Constants"

        /// <summary>
        /// Gets a value indicating whether edit mode.
        /// </summary>
        public bool EditMode
        {
            get
            {
                return this.PageManager.ViewMode == ViewModeEnum.Edit;
            }
        }

        #endregion

        #region "Methods"

        #endregion
    }
}