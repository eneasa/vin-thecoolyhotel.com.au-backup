// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.StoreDirectory
{
    using System;
    using System.Net;

    using Deepend.Framework.Kentico.Modules.ImageCrop;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Constants"

        /// <summary>
        /// Gets the max results.
        /// </summary>
        public override int MaxResults
        {
            get
            {
                return 1000;
            }
        }

        #endregion

        #region "Properties"

        /// <summary>
        /// The _store object.
        /// </summary>
        private string _storeObject;

        /// <summary>
        /// Gets the store object.
        /// </summary>
        public string StoreObject
        {
            get
            {
                if (this._storeObject == null)
                {
                    var result = string.Empty;

                    foreach (var item in this.Items)
                    {
                        result += "\"" + ((item.Name != null) ? WebUtility.HtmlEncode(item.Name) : string.Empty)
                                  + "\" : { " + "MapId: \""
                                  + ((item.MapId != null) ? WebUtility.HtmlEncode(item.MapId.ToString()) : string.Empty)
                                  + "\", " + "PhoneNumber: \""
                                  + ((item.PhoneNumber != null) ? WebUtility.HtmlEncode(item.PhoneNumber) : string.Empty)
                                  + "\", " + "DetailPage: \""
                                  + ((item.DetailPage != null) ? WebUtility.HtmlEncode(item.DetailPage) : string.Empty)
                                  + "\", " + "LogoUrl: \""
                                  + ((item.LogoGuid != null && item.LogoGuid != Guid.Empty)
                                         ? WebUtility.HtmlEncode(ImageCropUtils.GetImageUrl(item.LogoGuid, "Originals"))
                                         : string.Empty) + "\", " + "InfoContent: \""
                                  + ((item.InfoText != null)
                                         ? WebUtility.HtmlEncode(item.InfoText.Replace(Environment.NewLine, string.Empty))
                                         : string.Empty) + "\" " + "},";
                    }

                    this._storeObject = result.Trim(',');
                }

                return this._storeObject;
            }
        }

        #endregion
    }
}