// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.StoreDirectory
{
    using CMS.Helpers;

    using Deepend.Framework.Kentico.WebPart;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart, StoreModel>
    {
        #region "WebPart Property Names"

        /// <summary>
        /// Gets the pro p_ communit y_ id.
        /// </summary>
        public virtual string PROP_COMMUNITY_ID
        {
            get
            {
                return "CommunityId";
            }
        }

        /// <summary>
        /// Gets the pro p_ de v_ key.
        /// </summary>
        public virtual string PROP_DEV_KEY
        {
            get
            {
                return "DevKey";
            }
        }

        #endregion

        #region "WebPart Default Values"

        /// <summary>
        /// Gets the defaul t_ communit y_ id.
        /// </summary>
        public virtual string DEFAULT_COMMUNITY_ID
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the defaul t_ de v_ key.
        /// </summary>
        public virtual string DEFAULT_DEV_KEY
        {
            get
            {
                return string.Empty;
            }
        }

        #endregion

        #region "WebPart Properties"

        /// <summary>
        /// Gets or sets the community id.
        /// </summary>
        public string CommunityId
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_COMMUNITY_ID), 
                    this.DEFAULT_COMMUNITY_ID, 
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_COMMUNITY_ID, value);
            }
        }

        /// <summary>
        /// Gets or sets the dev key.
        /// </summary>
        public string DevKey
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_DEV_KEY), 
                    this.DEFAULT_DEV_KEY, 
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_DEV_KEY, value);
            }
        }

        #endregion
    }
}