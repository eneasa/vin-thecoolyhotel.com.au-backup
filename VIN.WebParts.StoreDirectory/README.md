﻿# WebPart Development
Modules that inherit from the Deepend.Framework.Kentico.WebPart base, gain access to an array of features, and benefits.
Javascript should be placed at __~/static/js/webparts/[UniqueIdentifier].js__ for auto-loaded scripts, or __static/js/adhoc/[UniqueIdentifier].js__ for manually loaded scripts.
Views should be placed at __~/Views/[WebPartName]/[ViewName].cshtml__

## Kentico Properties
These properties can be mapped to the WebPart to expose them as a configuration option within the CMS

### Common Properties
* __Template__(required) - The path to the template inside the __Views/[ProjectName]__ folder that you wish to render
* __CacheTime__ - The length of time to cache a module for
* __EnableShuffle__ - Shuffle the order of items (_Default: false_)
* __EnableFeaturedNode__ - Enable Featured Node functionality (_Default: true_)
* __EnableAutoHide__ - Automatically hide empty modules (_Default: true_)
* __EnableStripModuleWhitespace__ - Remove additional whitespace from rendered output (_Default: true_)

### List View Kentico Properties
* __InventoryRoot__ - The default path to search for items within
* __ItemFilter__ - A list of content-type-specific tags to filter by when narrowing down the list of displayed items
* __OrderBy__ - A SQL query to order by (_Default: NodeLevel, NodeOrder, DocumentName_)
* __MaxResults__ - The maximum number of items to display (_Default: 36_)

### Detail View Kentico Properties
* __FeaturedItem__ - the NodeId of a single item to display
* __EnableFirstItem__ - Enable First Item functionality (_Default: true_)
* __EnableURLDetect__ - Enable URL Detection functionality (_Default: true_)
* __Enable404Redirect__ - Redirect to 404 page when URL Detection fails (_Default: true_)
* __ShuffleIndex__ - The index of the current item, when utilizing a shuffled order

## Template Properties
The template is rendered within the context of the WebPart itself. This means it has access to all of the __Kentico Properties__ above, as well as the following additional properties

### Common Properties
* __CurrentAlias__ - The alias of the currently rendered webpage
* __CurrentDocument__ - The Kentico TreeNode object representing the current page
* __CurrentNode__ - The Kentico TreeNode object representing the current inventory item, or current page if no inventory item is found
* __CurrentSite__ - The alias of the currently rendered webpage
* __CurrentSiteName__ - The alias of the currently rendered webpage
* __CurrentPageInfo__ - The alias of the currently rendered webpage
* __CurrentUser__ - The alias of the currently rendered webpage

### List View Properties
* __Items__ - A filtered list of items that match the inbuilt tag filters

### Detail View Properties
* __Item__ - The FeaturedNodeId, or the item currently referenced in the current URL

## URL Parameters
When configuring pages in the CMS, you may wish to add a custom __Document URL Path__ to a page.

When utilizing this feature, it is possible to add URL or Query parameters to the URL, e.g. _/Site/News/__{Category}___

These parameters will become available via the __this.Request.Params__ collection e.g. _this.Request.Params[__"Category"__]_

# Domain Model Development
* All DomainModels based on the InventoryModel should be split into __[Name]Model.cs__ and __[Name]Model.Kentico.cs__.
* The __[Name]Model.Kentico.cs__ file should include the __KenticoDocType__ attribute, and should ONLY contain properties with basic get/set methods, which should directly map to the properties in the Kentico DocType. This file will be auto-generated in future, so modifications will be lost if this convention is ignored.
* The __[Name]Model.Kentico.cs__ file will inherit from __InventoryModel__, and __IInventoryModel__
* The __[Name]Model.cs__ file will not contain any Kentico DocType properties, and will not inherit from any classes, although it may implement interfaces if desired.
* The two files should both be partial classes of the same name

# Database Updates
Kentico up to 8.0.21 had a bug with Persona Support. To correct this, the 2 Stored Procs relating to Personas in the CRM database need to have the ExpirationDate <= GetDate() Logic reversed to >= GetDate()