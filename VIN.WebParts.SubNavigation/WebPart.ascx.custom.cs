// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.SubNavigation
{
    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Properties"

        /// <summary>
        /// Gets the max results.
        /// </summary>
        public override int MaxResults
        {
            get
            {
                return 4;
            }
        }

        #endregion

        #region "Methods"

        #endregion
    }
}