// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.Navigation
{
    using Deepend.Framework.Kentico.WebParts.Navigation;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPart<WebPart>
    {
        #region "WebPart Default Values"

        #endregion

        #region "WebPart Properties"

        #endregion
    }
}