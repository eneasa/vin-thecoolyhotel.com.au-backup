// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.Navigation
{
    using System;

    using CMS.CustomTables;
    using CMS.DataEngine;
    using System.Collections.Generic;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Constants"

        /*private void nothing()
        {
            //this.RootNode
            //this.FilteredItems
            //this.DocumentsOnPath
            //this.GetChildren_Cached(nodeId, optionalFilterUsedForFooter)
            //this.GetFilteredItems_Cached() - Should not need
        }*/
        #endregion

        #region "Properties"

        /// <summary>
        /// The _open times.
        /// </summary>
        private string _openTimes;

        private Dictionary<string, string> _categories;
        /// <summary>
        /// Gets the open times.
        /// </summary>
        public string OpenTimes
        {
            get
            {
                if (this._openTimes == null)
                {
                    var openTimes = string.Empty;

                    var specificItem =
                        CustomTableItemProvider.GetItems("VIN.OpenTimes")
                            .WhereEquals("Day", "specific")
                            .Where("StartDate", QueryOperator.LessOrEquals, DateTime.Now.ToString("yyyy-MM-dd"))
                            .Where("EndDate", QueryOperator.LargerOrEquals, DateTime.Now.ToString("yyyy-MM-dd"))
                            .FirstObject;

                    if (specificItem != null)
                    {
                        openTimes = specificItem.GetValue("StartTime", string.Empty) + " - "
                                    + specificItem.GetValue("EndTime", string.Empty);
                    }

                    if (string.IsNullOrEmpty(openTimes))
                    {
                        var item =
                            CustomTableItemProvider.GetItems("VIN.OpenTimes")
                                .WhereEquals("Day", DateTime.Now.DayOfWeek.ToString().ToLower())
                                .FirstObject;

                        if (item != null)
                        {
                            openTimes = item.GetValue("StartTime", string.Empty) + " - "
                                        + item.GetValue("EndTime", string.Empty);
                        }
                    }

                    this._openTimes = openTimes;
                }

                return this._openTimes;
            }
        }

        public Dictionary<string, string> Categories
        {
            get
            {
                if (this._categories == null)
                {
                    IEnumerable<CustomTableItem> catItems =
                        CustomTableItemProvider.GetItems("VIN.EmailCategories");
                    var result = new Dictionary<string, string>();

                    foreach (var catItem in catItems)
                    {
                        result.Add(
                            catItem.GetValue("CategoryValue", string.Empty),
                            catItem.GetValue("CategoryName", string.Empty));
                    }

                    this._categories = result;
                }

                return this._categories;
            }
        }
        #endregion
    }
}