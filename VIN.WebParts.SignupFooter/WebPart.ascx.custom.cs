// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.SignupFooter
{
    using System.Collections.Generic;

    using CMS.CustomTables;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Properties"

        /// <summary>
        /// The _categories.
        /// </summary>
        private Dictionary<string, string> _categories;

        /// <summary>
        /// Gets the categories.
        /// </summary>
        public Dictionary<string, string> Categories
        {
            get
            {
                if (this._categories == null)
                {
                    IEnumerable<CustomTableItem> catItems =
                        CustomTableItemProvider.GetItems("VIN.EmailCategories");
                    var result = new Dictionary<string, string>();

                    foreach (var catItem in catItems)
                    {
                        result.Add(
                            catItem.GetValue("CategoryValue", string.Empty), 
                            catItem.GetValue("CategoryName", string.Empty));
                    }

                    this._categories = result;
                }

                return this._categories;
            }
        }

        #endregion

        #region "Methods"

        #endregion
    }
}