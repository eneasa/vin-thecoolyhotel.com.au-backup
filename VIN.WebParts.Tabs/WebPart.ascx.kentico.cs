// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.Tabs
{
    using Deepend.Framework.Kentico.WebPart;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart, TabModel>
    {
        #region "WebPart Default Values"

        #endregion

        #region "WebPart Properties"

        #endregion
    }
}