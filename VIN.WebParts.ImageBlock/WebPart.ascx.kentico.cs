// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.ImageBlock
{
    using System;

    using CMS.Helpers;

    using Deepend.Framework.Kentico.WebPart;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart>
    {
        #region "WebPart Property Names"

        /// <summary>
        /// Gets the pro p_ title.
        /// </summary>
        public virtual string PROP_TITLE
        {
            get
            {
                return "Title";
            }
        }

        /// <summary>
        /// Gets the pro p_ imag e_ guid.
        /// </summary>
        public virtual string PROP_IMAGE_GUID
        {
            get
            {
                return "ImageGuid";
            }
        }

        /// <summary>
        /// Gets the pro p_ pa n_ zoom.
        /// </summary>
        public virtual string PROP_PAN_ZOOM
        {
            get
            {
                return "PanZoom";
            }
        }

        #endregion

        #region "WebPart Default Values"

        /// <summary>
        /// Gets the defaul t_ title.
        /// </summary>
        public virtual string DEFAULT_TITLE
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the defaul t_ imag e_ guid.
        /// </summary>
        public virtual Guid DEFAULT_IMAGE_GUID
        {
            get
            {
                return Guid.Empty;
            }
        }

        /// <summary>
        /// Gets a value indicating whether defaul t_ pa n_ zoom.
        /// </summary>
        public virtual bool DEFAULT_PAN_ZOOM
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region "WebPart Properties"

        /// <summary>
        /// Gets or sets the title.
        /// </summary>
        public string Title
        {
            get
            {
                return ValidationHelper.GetString(this.GetValue(this.PROP_TITLE), this.DEFAULT_TITLE, this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_TITLE, value);
            }
        }

        /// <summary>
        /// Gets or sets the image guid.
        /// </summary>
        public Guid ImageGuid
        {
            get
            {
                return ValidationHelper.GetGuid(
                    this.GetValue(this.PROP_IMAGE_GUID), 
                    this.DEFAULT_IMAGE_GUID, 
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_IMAGE_GUID, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether pan zoom.
        /// </summary>
        public bool PanZoom
        {
            get
            {
                return ValidationHelper.GetBoolean(
                    this.GetValue(this.PROP_PAN_ZOOM), 
                    this.DEFAULT_PAN_ZOOM, 
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_PAN_ZOOM, value);
            }
        }

        #endregion
    }
}