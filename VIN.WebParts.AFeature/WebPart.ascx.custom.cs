// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------
namespace VIN.WebParts.AFeature
{
    using System.Collections.Generic;
    using System.Linq;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Methods"

        /// <summary>
        /// The get child a feature slides.
        /// </summary>
        /// <param name="items">
        /// The items.
        /// </param>
        /// <returns>
        /// The <see cref="List"/>.
        /// </returns>
        private List<AFeatureSlideModel> GetChildAFeatureSlides(List<AFeatureSlideModel> items)
        {
            var parentNodeId = this.CurrentDocument.NodeID;
            var slides = new List<AFeatureSlideModel>();

            slides.AddRange(items.Where(x => x.NodeParentID == parentNodeId));

            return slides;
        }

        #endregion

        /// <summary>
        /// Gets the items.
        /// </summary>
        public override List<AFeatureSlideModel> Items 
        {
            get
            {
                return this.GetChildAFeatureSlides(base.Items);
            }
        }

        #region "Properties"

        /// <summary>
        /// Gets the inventory root.
        /// </summary>
        public override string InventoryRoot
        {
            get
            {
                return this.CurrentDocument.NodeAliasPath;
            }
        }

        /// <summary>
        /// Gets a value indicating whether enable url detect.
        /// </summary>
        public override bool EnableURLDetect
        {
            get
            {
                return false;
            }
        }
        #endregion
    }
}