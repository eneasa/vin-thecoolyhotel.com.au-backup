// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.Breadcrumbs
{
    using System.Collections.Generic;

    using CMS.DocumentEngine;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Constants"

        /// <summary>
        /// The _breadcrumbs.
        /// </summary>
        private List<Dictionary<string, string>> _breadcrumbs;

        /// <summary>
        /// Gets the breadcrumbs.
        /// </summary>
        public List<Dictionary<string, string>> Breadcrumbs
        {
            get
            {
                if (this._breadcrumbs == null)
                {
                    this._breadcrumbs = new List<Dictionary<string, string>>();
                    var currentDocument = DocumentContext.CurrentDocument;

                    if (!currentDocument.IsRoot())
                    {
                        var cDoc = new Dictionary<string, string>();
                        cDoc.Add(
                            DocumentContext.CurrentDocument.DocumentName, 
                            DocumentContext.CurrentDocument.AbsoluteURL);
                        this._breadcrumbs.Add(cDoc);
                    }

                    while (!currentDocument.IsRoot())
                    {
                        currentDocument = currentDocument.Parent;

                        if (currentDocument.IsRoot())
                        {
                            var cDoc = new Dictionary<string, string>();
                            cDoc.Add("Home", "/");
                            this._breadcrumbs.Add(cDoc);
                        }
                        else if (currentDocument.ClassName == "CMS.MenuItem")
                        {
                            var cDoc = new Dictionary<string, string>();
                            cDoc.Add(currentDocument.DocumentName, currentDocument.AbsoluteURL);
                            this._breadcrumbs.Add(cDoc);
                        }
                    }
                }

                this._breadcrumbs.Reverse();

                return this._breadcrumbs;
            }
        }

        #endregion

        #region "Methods"

        #endregion
    }
}