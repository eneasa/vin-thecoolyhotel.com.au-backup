// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.RelatedBlogPosts
{
    using Deepend.Framework.Kentico.WebPart;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart, BlogPostModel>
    {
        // WebPartBase<WebPart, DomainType>
        #region "WebPart Default Values"

        #endregion

        #region "WebPart Properties"

        #endregion
    }
}