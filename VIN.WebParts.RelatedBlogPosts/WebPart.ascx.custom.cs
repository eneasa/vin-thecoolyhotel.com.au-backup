// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.RelatedBlogPosts
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    using CMS.DocumentEngine;
    using CMS.PortalEngine;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        /// <summary>
        /// The _page items.
        /// </summary>
        private List<BlogPostModel> _pageItems;

        /// <summary>
        /// Gets a value indicating whether edit mode.
        /// </summary>
        public bool EditMode
        {
            get
            {
                return this.PageManager.ViewMode == ViewModeEnum.Edit;
            }
        }

        /// <summary>
        /// Gets the inventory root.
        /// </summary>
        public override string InventoryRoot
        {
            get
            {
                return this.CurrentDocument.NodeAliasPath;
            }
        }

        /// <summary>
        /// Gets the tag value.
        /// </summary>
        public string TagValue
        {
            get
            {
                var tag = HttpContext.Current.Request.Params["tag"];

                return (!string.IsNullOrEmpty(tag)) ? HttpUtility.UrlDecode(tag) : string.Empty;
            }
        }

        /// <summary>
        /// Gets the page items.
        /// </summary>
        public List<BlogPostModel> PageItems
        {
            get
            {
                if (this._pageItems == null)
                {
                    var InventoryRoot = (!string.IsNullOrEmpty(this.TagValue))
                                            ? this.CurrentDocument.Parent.NodeAliasPath
                                            : this.CurrentDocument.NodeAliasPath;

                    // String InventoryRoot = this.CurrentDocument.NodeAliasPath;
                    var blogPost = this.CurrentDocument.ToDomainModel<BlogPostModel>();

                    if (!string.IsNullOrEmpty(blogPost.DocumentTags))
                    {
                        var result = DocumentHelper.GetDocuments(BlogPostModel.KENTICO_CLASSNAME)

                            // .Path(InventoryRoot, PathTypeEnum.Section)
                            .Published()
                            .Culture(this.CurrentDocument.DocumentCulture)
                            .TypedResult.Items.ToDomainModels<BlogPostModel>()
                            .Where(x => x.DocumentID != blogPost.DocumentID)
                            .OrderBy(x => x.BlogPostDate)
                            .ToList();

                        var blogPostTags =
                            Array.ConvertAll(
                                blogPost.DocumentTags.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries), 
                                p => p.TrimStart());

                        var tagMatchs = new List<BlogPostModel>();

                        foreach (var model in result.Where(x => x.NodeID != this.CurrentDocument.NodeID))
                        {
                            if (model.DocumentTags == null)
                            {
                                continue;
                            }

                            var resultTags =
                                Array.ConvertAll(
                                    model.DocumentTags.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries), 
                                    p => p.TrimStart());

                            if (resultTags.Any() && resultTags.Intersect(blogPostTags).Any())
                            {
                                tagMatchs.Add(model);
                            }
                        }

                        // result =
                        // result.Where(
                        // x =>
                        // x.DocumentTags != null && x.DocumentTags.Contains(blogPost.DocumentTags) &&
                        // x.NodeID != CurrentDocument.NodeID).ToList();

                        // this._pageItems = result.Take(3);
                        this._pageItems = tagMatchs;
                    }
                }

                return this._pageItems ?? new List<BlogPostModel>();
            }
        }

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            base.OnLoad(e);
            var s = this.Items;

            // if (this._pageItems == null)
            // {
            // String InventoryRoot = this.CurrentDocument.NodeAliasPath;
            // var tp = new TreeProvider();
            // var doc = DocumentHelper.GetDocument(this.CurrentDocument.DocumentID, tp);
            // var blogPost = doc.ToDomainModel<BlogPostModel>();

            // if (!string.IsNullOrEmpty(blogPost.DocumentTags))
            // {
            // List<BlogPostModel> result = DocumentHelper.GetDocuments(BlogPostModel.KENTICO_CLASSNAME)
            // .Published()
            // .Culture(this.CurrentDocument.DocumentCulture)
            // .TypedResult
            // .Items
            // .ToDomainModels<BlogPostModel>()
            // .OrderBy(x => x.BlogPostDate)
            // .ToList();

            // result =
            // result.Where(x => x.DocumentTags != null && x.DocumentTags.Contains(blogPost.DocumentTags))
            // .ToList();

            // this._pageItems = result;
            // }
            // }
        }
    }
}