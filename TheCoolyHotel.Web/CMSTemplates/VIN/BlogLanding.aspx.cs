// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BlogLanding.aspx.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The cms templates blog landing.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System;

using CMS.UIControls;

/// <summary>
/// The cms templates blog landing.
/// </summary>
public partial class CMSTemplatesBlogLanding : TemplatePage
{
    /// <summary>
    /// The page_ load.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }
}