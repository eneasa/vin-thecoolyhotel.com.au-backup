﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/CMSMasterPages/Master/Main.master" CodeBehind="ContactUs.aspx.cs" Inherits="TheCoolyHotel.Web.CMSTemplates.VIN.ContactUs" %>

<asp:Content ID="Content" ContentPlaceHolderID="PageContent" runat="Server">
    <cms:CMSPagePlaceholder ID="CMSPagePlaceholder" runat="server">
        <LayoutTemplate>
            <div id="master-container">
                <header id="master-header">
                    <div class="w-nav navbar" id="topnav" data-collapse="small" data-animation="default" data-duration="400" data-doc-height="1">
                        <cms:CMSWebPartZone ZoneId="Header" runat="server" />
                    </div>
                </header>

                <div id="master-content">
                    <div class="content-zone container-fluid">
                        <section class="content-section contact-us-page-content">

							<div class="map-wrap">

								<div class="container-fluid">
									<div class="row">
										<div class="contact-us-breadcrumb">
                                            <div class="w-container">
											    <cms:CMSWebPartZone ZoneId="Content Row 1" runat="server" />
											    <span class="contact-page-title">CONTACT US</span>
                                            </div>
                                        </div>

                                        <div>
                                            <cms:CMSWebPartZone ZoneId="Content Row 2" runat="server" />
                                        </div>

                                    </div>
							    </div>

							    <div class="contact-us-section">
								    <div class="w-row">
									    <div class="row">
										    <cms:CMSWebPartZone ZoneId="Content Row 3" runat="server" />
									    </div>
								    </div>
							    </div>
                            </div>

                        </section>

                    </div>
                </div>
                <footer id="master-footer">
                    <cms:CMSWebPartZone ID="CMSWebPartZone1" ZoneId="Footer" runat="server" />
                </footer>
            </div>
        </LayoutTemplate>
    </cms:CMSPagePlaceholder>
</asp:Content>
