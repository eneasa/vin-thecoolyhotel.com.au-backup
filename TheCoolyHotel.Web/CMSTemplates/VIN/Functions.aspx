﻿<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/Master/Main.master" AutoEventWireup="true" Inherits="CMSTemplatesFunctions" Title="Untitled Page" ValidateRequest="false" Codebehind="Functions.aspx.cs" %>

<asp:Content ID="Content" ContentPlaceHolderID="PageContent" runat="Server">
	<cms:CMSPagePlaceholder ID="CMSPagePlaceholder" runat="server">
		<LayoutTemplate>
			<div id="master-container">
                <header id="master-header">
                    <div class="w-nav navbar" id="topnav" data-collapse="small" data-animation="default" data-duration="400" data-doc-height="1">
                        <cms:CMSWebPartZone ID="CMSWebPartZone1" ZoneID="Header" runat="server" />
                    </div>
                </header>

			    <div id="master-content">
			    	<div class="hero-zone">
                        <section>
                            <cms:CMSWebPartZone ID="CMSWebPartZone2" ZoneID="Hero" runat="server" />
                        </section>
                    </div>

			        <div class="content-zone container-fluid">
                        <!-- API CONTENT CONTAINER START -->
			        	<div class="row">
				        	<div class="col-lg-8">

                                <section class="content-section">
                                    <div class="w-container">

                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ID="CMSWebPartZone4" ZoneID="Content Row 2" runat="server" />
					                        </div>
                                        </div>
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ID="CMSWebPartZone5" ZoneID="Content Row 3" runat="server" />
					                        </div>
                                        </div>
                                    </div>
                                    <div class="grey-section">
                                        <div class="w-container">
                                            <div class="w-row">
					                            <div class="row">
					                                <cms:CMSWebPartZone ID="CMSWebPartZone6" ZoneID="Content Row 4" runat="server" />
					                            </div>
                                            </div>
                                        </div>
                                    </div>

                                </section>
					        </div>
						</div>

                        <div class="row">
                            <footer class="footer">
                                <cms:CMSWebPartZone ID="CMSWebPartZone15" ZoneID="Page Footer Zone" runat="server" />
                            </footer>
			    		</div>
                        <!-- API CONTENT CONTAINER END -->
			        </div>
			    </div>

			    <footer id="master-footer">
	                <cms:CMSWebPartZone ID="CMSWebPartZone16" ZoneID="Footer" runat="server" />
	            </footer>
			</div>
		</LayoutTemplate>
	</cms:CMSPagePlaceholder>
</asp:Content>