<%@ Page Language="C#" MasterPageFile="~/CMSMasterPages/Master/Main.master" AutoEventWireup="true" Inherits="CMSTemplatesHome" Title="Untitled Page" ValidateRequest="false" Codebehind="Home.aspx.cs" %>

<asp:Content ID="Content" ContentPlaceHolderID="PageContent" runat="Server">
	<cms:CMSPagePlaceholder ID="CMSPagePlaceholder" runat="server">
		<LayoutTemplate>
			<div id="master-container">
                <header id="master-header">
                    <div class="w-nav navbar" id="topnav" data-collapse="small" data-animation="default" data-duration="400" data-doc-height="1">
                        <cms:CMSWebPartZone ZoneID="Header" runat="server" />
                    </div>
                </header>

			    <div id="master-content">
			    	<div class="hero-zone">
                        <section>
                            <cms:CMSWebPartZone ZoneID="Hero" runat="server" />
                        </section>
                    </div>

			        <div class="content-zone container-fluid">
                        <!-- API CONTENT CONTAINER START -->
			        	<div class="row">
				        	<div class="col-lg-8">

                                <section class="featured-section">
                                    <div class="w-container">
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 2" runat="server" />
					                        </div>
                                        </div>

                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 3" runat="server" />
					                        </div>
                                        </div>
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 4" runat="server" />
					                        </div>
                                        </div>
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 5" runat="server" />
					                        </div>
                                        </div>
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 6" runat="server" />
					                        </div>
                                        </div>
                                    </div>
                                </section>

                                <section class="darker-section">
                                    <div class="w-container">
                                        <div class="w-row">
                                            <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 7" runat="server" />
					                        </div>
                                        </div>
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 8" runat="server" />
					                        </div>
                                        </div>
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 9" runat="server" />
					                        </div>
                                        </div>
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 10" runat="server" />
					                        </div>
                                        </div>
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 11" runat="server" />
					                        </div>
                                        </div>
                                        <div class="w-row">
					                        <div class="row">
					                            <cms:CMSWebPartZone ZoneID="Content Row 12" runat="server" />
					                        </div>
                                        </div>
                                    </div>
                                </section>
					        </div>
						</div>

                        <div class="row">
                            <footer class="footer">
                                <cms:CMSWebPartZone ID="CMSWebPartZone3" ZoneID="Page Footer Zone" runat="server" />
                            </footer>
			    		</div>
                        <!-- API CONTENT CONTAINER END -->
			        </div>
			    </div>

			    <footer id="master-footer">
	                <cms:CMSWebPartZone ID="CMSWebPartZone1" ZoneID="Footer" runat="server" />
	            </footer>
			</div>
		</LayoutTemplate>
	</cms:CMSPagePlaceholder>
</asp:Content>