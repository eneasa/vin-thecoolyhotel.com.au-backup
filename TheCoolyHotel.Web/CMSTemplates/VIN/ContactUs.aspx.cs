﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ContactUs.aspx.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The contact us.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Web.CMSTemplates.VIN
{
    using System;

    using CMS.UIControls;

    /// <summary>
    /// The contact us.
    /// </summary>
    public partial class ContactUs : TemplatePage
    {
        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {

        }
    }
}