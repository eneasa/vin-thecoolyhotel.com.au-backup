// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Child.aspx.cs" company="">
//   
// </copyright>
// <summary>
//   The cms templates child.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System;

using CMS.UIControls;

/// <summary>
/// The cms templates child.
/// </summary>
public partial class CMSTemplatesChild : TemplatePage
{
    /// <summary>
    /// The page_ load.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }
}