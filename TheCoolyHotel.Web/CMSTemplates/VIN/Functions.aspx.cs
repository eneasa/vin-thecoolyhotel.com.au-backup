﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Functions.aspx.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The cms templates child.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System;

using CMS.UIControls;

/// <summary>
/// The cms templates Functions.
/// </summary>
public partial class CMSTemplatesFunctions : TemplatePage
{
    /// <summary>
    /// The page_ load.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void Page_Load(object sender, EventArgs e)
    {
    }
}