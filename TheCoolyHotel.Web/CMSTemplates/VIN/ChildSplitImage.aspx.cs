// --------------------------------------------------------------------------------------------------------------------
// <copyright file="ChildSplitImage.aspx.cs" company="Deepend">
//   
// </copyright>
// <summary>
//   The cms templates child split image.
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System;
using System.Web.UI.WebControls;

using CMS.DocumentEngine;
using CMS.UIControls;

using Deepend.Framework.Kentico.Modules.ImageCrop;

/// <summary>
/// The cms templates child split image.
/// </summary>
public partial class CMSTemplatesChildSplitImage : TemplatePage
{
    /// <summary>
    /// The page_ load.
    /// </summary>
    /// <param name="sender">
    /// The sender.
    /// </param>
    /// <param name="e">
    /// The e.
    /// </param>
    protected void Page_Load(object sender, EventArgs e)
    {
        if (DocumentContext.CurrentDocument != null)
        {
            var currentDocument = DocumentContext.CurrentDocument;
            Panel ImageBgPnl = (Panel)CMSPagePlaceholder.FindControl("pnlPage:lt:ImageBgPnl");

            if (ImageBgPnl != null)
            {
                Guid backgroundImage = currentDocument.GetGuidValue("BackgroundImage", Guid.Empty);
                Guid backgroundImageMobile = currentDocument.GetGuidValue("BackgroundImageMobile", Guid.Empty);

                if (backgroundImage != Guid.Empty)
                {
                    string bgUrl = ImageCropUtils.GetImageUrl(backgroundImage, "Original");
                    ImageBgPnl.Attributes.Add("data-background-image", bgUrl);
                }

                if (backgroundImageMobile != Guid.Empty)
                {
                    string bgMobileUrl = ImageCropUtils.GetImageUrl(backgroundImageMobile, "Original");
                    ImageBgPnl.Attributes.Add("data-background-image-mobile", bgMobileUrl);
                }
            }
        }
    }
}