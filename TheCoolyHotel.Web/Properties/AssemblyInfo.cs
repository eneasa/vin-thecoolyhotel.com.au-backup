﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="AssemblyInfo.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   AssemblyInfo.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------



using System.Reflection;
using System.Runtime.InteropServices;

using CMS;

using Deepend.Framework.Attributes;

// Required for Kentico to pick up our assembly
[assembly: AssemblyDiscoverable]
[assembly: DeependAssembly]
[assembly: AssemblyTitle("VIN.Web")]
[assembly: AssemblyDescription("VIN.Web")]
[assembly: ComVisible(false)]
[assembly: Guid("a30aad0c-f868-4676-a169-9199e7c6cfe0")]