﻿<%@ Control Language="C#" AutoEventWireup="true" Codebehind="MetaFileListControl.ascx.cs" Inherits="CMSFormControls_Basic_MetaFileListControl" %>
<%@ Register Src="~/CMSModules/AdminControls/Controls/MetaFiles/FileList.ascx" TagName="FileList" TagPrefix="cms" %>

<cms:FileList ID="fileList" runat="server" ShortID="fl" />

