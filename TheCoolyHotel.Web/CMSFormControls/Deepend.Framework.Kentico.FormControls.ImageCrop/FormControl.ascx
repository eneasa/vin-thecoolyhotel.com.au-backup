<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="FormControl.ascx.cs" Inherits="Deepend.Framework.Kentico.FormControls.ImageCrop.FormControl" %>
<%@ Register Assembly="CMS.Controls" Namespace="CMS.Controls" TagPrefix="cms" %>

<cms:MediaSelector ID="mediaSelector" runat="server" ShowClearButton="true" />

<asp:Panel ID="pnlCrop" runat="server">
    <asp:Image id="imgCrop" style="max-width: 400px; max-height: 400px;" AlternateText="Preview" runat="server" />
    <i runat="server" id="defaultImage" aria-hidden="true" class="icon-file-default icon-file-default cms-icon-100"></i>
</asp:Panel>