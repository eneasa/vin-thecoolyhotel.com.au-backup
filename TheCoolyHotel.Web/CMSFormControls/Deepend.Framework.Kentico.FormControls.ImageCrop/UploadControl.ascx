﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="UploadControl.ascx.cs" Inherits="Deepend.Framework.Kentico.FormControls.ImageCrop.UploadControl" %>
<%@ Register Assembly="CMS.ExtendedControls" Namespace="CMS.ExtendedControls" TagPrefix="cms" %>

<asp:HiddenField ID="hidFocalPoint" runat="server" />

<cms:uploader id="uploader" runat="server" />
<asp:Button ID="hdnPostback" CssClass="HiddenButton" runat="server" EnableViewState="false" />

<asp:Panel ID="pnlCrop" runat="server">
    <div runat="server" id="divImage" class="divImage">
        <asp:Image ID="imgCrop" AlternateText="Preview" runat="server" CssClass="imgCrop" />
        <div runat="server" id="divMarker" class="ic_marker"><span></span></div>
    </div>
    <asp:Button ID="btnFullscreen" runat="server" Text="Zoom" />
</asp:Panel>
