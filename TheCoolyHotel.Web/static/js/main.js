﻿(function ($) {

    $(function () {

        /********************************************
        Mobile menu search functions
        ********************************************/
        defaultURL = window.location.host;
        searchURL = defaultURL + "/search-results?q=";
        searchQuery = "";

        function submitSearch(searchString) {
            searchResultsUrl = "http://" + searchURL + searchString;
            window.location = searchResultsUrl;
        }

        //Search DOM elements
        searchSubmitBtn = $(".mobile-menu .mobile-search-block .search-btn");
        searchInput = $(".mobile-menu .mobile-search-block .mobile-search");

        //Search vars


        searchSubmitBtn.click(function () {
            searchQuery = searchInput.val();
            submitSearch(searchQuery);
        });

        var searchOpen = false;

        $(".search-container .search-btn").click(function () {
            var toggleWidth = $(".search-container").width() == 66 ? "250px" : "66px";

            var searchInput = $("#Search");
            var searchValue = searchInput.val();

            if (searchOpen == false) {

                $(".search-container").animate({ width: toggleWidth });
                searchOpen = true;

            } else {

                if (searchValue == "") {
                    $(".search-container").animate({ width: toggleWidth });
                    searchOpen = true;
                } else {
                    submitSearch(searchValue);
                }
            }
        });

        $("#first-return").click(function () {
            $("#first-tab").click();
        });
        $("#second-return").click(function () {
            $("#second-tab").click();
        });
        $("#third-return").click(function () {
            $("#third-tab").click();
        });
        $("#fourth-return").click(function () {
            $("#fourth-tab").click();
        });

        $("#top .email-signup, .mail-section .mailclose").click(function (e) {
            e.preventDefault();
            $("#master-header .mail-section").slideToggle();
        });

        $(".header-signup-button").click(function (e) {
            e.preventDefault();

            var form = $(this).parents(".mail-form");
            var email = form.find("input.w-input").val();

            MailSignup(email, "");
        });

        $(".footer-signup-button").click(function (e) {
            e.preventDefault();

            var form = $(this).parents(".mail-form");
            var email = form.find("input.w-input").val();
            var categories = "";

            form.find(".w-checkbox-input:checked").each(function () {
                categories += $(this).val() + "|";
            });

            MailSignup(email, categories);
        });

        $(".module-signup-button").click(function (e) {
            e.preventDefault();

            var form = $(this).parents(".mail-form");
            var email = form.find("input.w-input").val();

            MailSignup(email, "");
        });

        /* Social Module */
        function popupwindow(url, w, h) {

            var screenWidth = $(window).width();
            var screenHeight = $(window).height();
            var left = (screenWidth / 2) - (w / 2);
            var top = (screenHeight / 2) - (h / 2);

            window.open(url, "mywindow", "menubar=1,resizable=1,width=" + w + ",height=" + h + ", top=" + top + ", left=" + left);
        }


        $(".content-zone .social-link.icon-facebook").click(function (e) {
            e.preventDefault();

            var docUrl;
            if (!/^(f|ht)tps?:\/\//i.test($(this).attr("href"))) {
                docUrl = window.location.protocol + "//" +
                    window.location.hostname +
                    $(this).attr("href");
            } else {
                docUrl = $(this).attr("href");
            }

            var FBUrl = "https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(docUrl);
            popupwindow(FBUrl, 550, 450);

        });

        $(".content-zone .social-link.icon-twitter").click(function (e) {
            e.preventDefault();

            var docUrl;

            if (!/^(f|ht)tps?:\/\//i.test($(this).attr("href"))) {
                docUrl = window.location.protocol + "//" +
                    window.location.hostname +
                    $(this).attr("href");
            } else {
                docUrl = $(this).attr("href");
            }

            var TwitterUrl = "http://www.twitter.com/share?url=" + encodeURIComponent(docUrl);
            popupwindow(TwitterUrl, 550, 450);
        });


        /* SLIDER */

        var mySwiper = new Swiper(".module-slider-enabled .swiper-container", {
            direction: "horizontal",
            loop: true,
            autoplay: 5000,
            autoplayDisableOnInteraction: true,
            slidesPerView: 3,
            centeredSlides: true,
            paginationClickable: true,

            onSlideChangeEnd: function (swiper) {

                var currentSlide = $(".swiper-container .swiper-slide-active");
                if (currentSlide.length == 0) {
                    currentSlide = $(".swiper-container .swiper-slide[data-swiper-slide-index='0']");
                }

                $(".module-slider .inner-box").html(
                    currentSlide.find(".item-content").html()
                );

                $(".video-button").colorbox({
                    iframe: true,
                    innerWidth: 640,
                    innerHeight: 390
                });

            },

            pagination: ".swiper-pagination"
        });

        /* Gallery Image Slider */

        var galleryTop = new Swiper('.module-image-gallery-enabled .gallery-top', {
            pagination: '.module-image-gallery-enabled .swiper-pagination',
            paginationClickable: true,
            nextButton: '.module-image-gallery-enabled .swiper-button-next',
            prevButton: '.module-image-gallery-enabled .swiper-button-prev',
            spaceBetween: 40,

        });
        var galleryThumbs = new Swiper('.module-image-gallery-enabled .gallery-thumbs', {
            spaceBetween: 0,
            centeredSlides: true,
            slidesPerView: 'auto',
            touchRatio: 0.2,
            slideToClickedSlide: true,
            paginationClickable: true,
        });
        galleryTop.params.control = galleryThumbs;
        galleryThumbs.params.control = galleryTop;

        /* Child Split Image Template */

        ImageBgResize();
        $(window).resize(function () {
            ImageBgResize();
        });

        /* Image Block Module - Zoom / Pan */

        if ($(".module-image-block").length > 0) {

            $(".module-image-block").each(function () {
                var $panzoom = $(this).find(".panzoom").panzoom();
                $panzoom.parent().on("mousewheel.focal", function (e) {
                    e.preventDefault();
                    var delta = e.delta || e.originalEvent.wheelDelta;
                    var zoomOut = delta ? delta < 0 : e.originalEvent.deltaY > 0;
                    $panzoom.panzoom("zoom", zoomOut, {
                        increment: 0.1,
                        animate: false,
                        focal: e
                    });
                });
            });

        }

    });

    function ImageBgResize() {
        $(".image-bg-section-container").each(function () {
            var desktopBackground = $(this).attr("data-background-image");
            var mobileBackground = $(this).attr("data-background-image-mobile");

            if ($(window).width() < 767) {
                $(this).css("background-image", "url(" + mobileBackground + ")");
            } else {
                $(this).css("background-image", "url(" + desktopBackground + ")");
            }

        });
    }

    $(document).on("click", ".w-lightbox-close", function (e) {
        e.preventDefault();
        $(".modal_overlay").hide();
    });

    function MailSignup(emailAddress, categories) {

        $(".w-form-done, .w-form-fail").hide();
        $(".w-form-loading").show();

        $.ajax({
            type: "POST",
            url: "/api/Subscribe/Signup",
            data: {
                Email: emailAddress,
                Categories: categories
            }
        })
            .done(function (response) {
                if (response == true) {
                    dataLayer.push({ 'event': "newsletterSignup" });
                    $(".w-form-done").show();
                } else {
                    $(".w-form-fail").show();
                }
            })
            .fail(function () {
                $(".w-form-fail").show();
            })
            .always(function () {
                $(".w-form-loading").hide();
            });
    }

    if ($(".module.blog-filter").length > 0) {
        initBlogFilter($(".module.blog-filter"));
    }

    /*Contact Us Map Block Accordion*/

    $(document).ready(function () {

        $(".contact-info-title").click(function () {
            var target = $(this).next(".module-info-content").toggleClass("module-info-open");

        });

    });

    $(document).ready(function () {

        $(".contact-info-title").click(function () {

            var target_arrow = $(this).find(".detail-arrow");
            console.log(target_arrow);

            console.log('fire');

            if (target_arrow.hasClass('icon-down-open')) {
                console.log('down');

                target_arrow.removeClass('icon-down-open').addClass('icon-up-open');

            } else if (target_arrow.hasClass('icon-up-open')) {

                console.log('up');
                target_arrow.removeClass('icon-up-open').addClass('icon-down-open');
            }
        });

    });


})(jq111);