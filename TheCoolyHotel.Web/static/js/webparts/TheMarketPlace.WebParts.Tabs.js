﻿+function($) {

    $(document).ready(function() {

        // Tab Module
        $(".tabs-menu .active-tab").click(function(e) {
            $(".tabs-menu .active-tab").removeClass("current-tab");
            $(this).addClass("current-tab");

            var FilterTag = $(this).attr("data-tag");
            var ModuleContainer = $(this).parents(".w-row").next(".w-row");

            if (FilterTag == "all") {
                ModuleContainer.find(".module-feature-tile").show();
            } else {
                ModuleContainer.find(".module-feature-tile").hide().each(function() {
                    if ($(this).attr("data-tag").indexOf(FilterTag) != -1) {
                        $(this).show();
                    }
                });
            }

        });

    });

}(jq111);