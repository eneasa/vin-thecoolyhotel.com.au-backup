//http://www.sitepoint.com/safe-console-log/
//Console.logs were causing errors in IE. This hack will just disregard the console.logs if the console object has not been instantiated (IE dev tools not opened)
(function(a) {
    function b() {}

    for (var c = "assert,count,debug,dir,dirxml,error,exception,group,groupCollapsed,groupEnd,info,log,markTimeline,profile,profileEnd,time,timeEnd,trace,warn".split(","), d; !!(d = c.pop());) {
        a[d] = a[d] || b;
    }
})
(function() {
    try {
        console.log();
        return window.console;
    } catch (a) {
        return (window.console = {});
    }
}());