// From http://stackoverflow.com/questions/1537848/jquery-plugin-namespace/11441340#11441340

(function($) {
    $.namespace = function(namespaceName, closures) {

        if ($.fn[namespaceName] === undefined) {
            $.fn[namespaceName] = function executor(context) {
                if (this instanceof executor) {
                    this.__context__ = context;
                } else {
                    return new executor(this);
                }
            };
        }

        $.each(closures, function(closureName, closure) {
            $.fn[namespaceName].prototype[closureName] = function() {

                // Don't act on absent elements -via Paul Irish's advice
                if (!this.__context__.length)
                    return;

                closure.apply(this.__context__, arguments);
            };
        });

    };
})(jq111);