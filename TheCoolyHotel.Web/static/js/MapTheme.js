
/*
    Main ("m"): The main fill of the object
    Outline ("o"): The stroke (outline) color of the object
    Text ("t"): The text color used to label the object (if any)
    Width ("w"): The width of the stroke
    Texture ("img"): The image to use as a tiled texture
    Shadow ("shadow"): The shadow to be applied to this object type
*/

var micelloTheme = {
    "s": {
        "Background": {
            "m": "#f8f8f8",
            "o": "#f8f8f8",
            "t": "#000000",
            "w": 2,
            "img": false,
            "shadow": false
        },
        "Section": {
            "m": "#ddd9d3",
            "o": "#f8f8f8",
            "t": "#ddd9d3",
            "w": 2,
            "img": false,
            "shadow": false
        },
        "Building": {
            "m": "#9bddd9",
            "o": "#f8f8f8",
            "t": "#000000",
            "w": 2,
            "img": false,
            "shadow": false
        },
        "Selected": {
            "m": "#8fe0da",
            "o": "#f8f8f8",
            "t": "#8fe0da",
            "w": 2,
            "img": false,
            "shadow": false
        },
        "Unit": {
            "m": "#ddd9d3",
            "o": "#f8f8f8",
            "t": "#ddd9d3",
            "w": 2,
            "img": false,
            "shadow": false
        },
        "Special": {
            "m": "#74c8ba",
            "o": "#f8f8f8",
            "t": "#ffffff",
            "w": 2,
            "img": false,
            "shadow": false
        },
        "Room": {
            "m": "#ddd9d3",
            "o": "#f8f8f8",
            "t": "#ddd9d3",
            "w": 2,
            "img": false,
            "shadow": false
        },
        "Level Change": {
            "m": "#b4e0af",
            "o": "#f8f8f8",
            "t": "#b4e0af",
            "w": 2,
            "img": false,
            "shadow": false
        },
        "Parking Structure": {
            "m": "#f8f8f8",
            "o": "#f8f8f8",
            "t": "#f8f8f8",
            "w": 2,
            "img": false,
            "shadow": false
        },
        "Inaccessible Space": {
            "m": "#cccccc",
            "o": "#f8f8f8",
            "t": "#cccccc",
            "w": 2,
            "img": false,
            "shadow": false
        }
    }
};