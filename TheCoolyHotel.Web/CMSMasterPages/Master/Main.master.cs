// --------------------------------------------------------------------------------------------------------------------
// <copyright file="Main.master.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The cms templates_ the marabhoon tavern.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace TheCoolyHotel.Web.CMSMasterPages.Master
{
    using System;
    using System.Web.UI.HtmlControls;

    using CMS.CustomTables;
    using CMS.DataEngine;
    using CMS.UIControls;
    
    public partial class CMSTemplates_Main : TemplateMasterPage
    {
        /// <summary>
        /// Gets or sets the node class.
        /// </summary>
        public string NodeClass { get; set; }

        /// <summary>
        /// The _open times meta.
        /// </summary>
        private string _openTimesMeta;

        /// <summary>
        /// Gets the open times meta.
        /// </summary>
        public string OpenTimesMeta
        {
            get
            {
                if (this._openTimesMeta == null)
                {
                    InfoObjectCollection<CustomTableItem> specificItems = CustomTableItemProvider
                        .GetItems("VIN.OpenTimes")
                        .WhereNot(new WhereCondition().Where("Day", QueryOperator.Equals, "specific"))
                        .TypedResult
                        .Items;

                    foreach (CustomTableItem specificItem in specificItems)
                    {
                        // <meta itemprop="openingHours" content="Mo 09:00-17:00">
                        string day = specificItem.GetValue("Day", string.Empty).ToString();
                        string shortDay = char.ToUpper(day[0]).ToString() + day[1].ToString();

                        this._openTimesMeta +=
                            "<meta itemprop=\"openingHours\" content=\"" +
                            shortDay + " " + 
                            specificItem.GetValue("StartTime", string.Empty).ToString() + "-" + 
                            specificItem.GetValue("EndTime", string.Empty).ToString() + "\">\n";
                    }
                }

                return this._openTimesMeta;
            }
        }

        /// <summary>
        /// The on pre render.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnPreRender(EventArgs e)
        {
            base.OnPreRender(e);

            this.litHeaderTags.Text = this.HeaderTags;


            if (this.CurrentPage != null)
            {
                this.NodeClass = string.IsNullOrWhiteSpace(this.CurrentPage.NodeAlias) ? "root" : "body-" + this.CurrentPage.NodeAlias.ToLowerInvariant();
            }

            if (this.PageManager.ViewMode == CMS.PortalEngine.ViewModeEnum.EditLive)
            {
                this.NodeClass += " EditMode";
            }

            // Kentico doesn't want to use the settings for the site page title on blogs
            if (this.CurrentPage.ClassName == "CMS.BlogPost" && !string.IsNullOrWhiteSpace(this.CurrentPage.DocumentPageTitle))
            {
                string newTitle = this.CurrentPage.DocumentPageTitle + " - The Coolangatta Hotel";
                this.Page.Header.Title = newTitle;
            }

            if (this.CurrentPage.ClassName == "CMS.Blog" ||
                this.CurrentPage.ClassName == "CMS.BlogMonth" ||
                this.CurrentPage.ClassName == "CMS.BlogPost")
            {
                HtmlLink canonical = new HtmlLink();
                canonical.Attributes.Add("rel", "canonical");
                canonical.Href = this.CurrentPage.AbsoluteURL;
                this.Page.Header.Controls.Add(canonical);
            }

        }

        /// <summary>
        /// The page_ load.
        /// </summary>
        /// <param name="sender">
        /// The sender.
        /// </param>
        /// <param name="e">
        /// The e.
        /// </param>
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Page.Header.DataBind();
        }
    }
}