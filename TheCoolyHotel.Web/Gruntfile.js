module.exports = function(grunt) {

    grunt.initConfig({
        'pkg': grunt.file.readJSON("package.json"),

        'jshint': {
            all: ["static/js/script.js"]
        },

        // REGEX REPLACE OPTIONS
        'regex-replace': {
            // Task to remap webpart javascript to the published directory
            remap: {
                src: ["static/js/**/*.map"],
                actions: [
                    {
                        name: "WebPartsJS",
                        search: "\\.\\./[^/]+\\.WebParts\\.[^/]+?/static/js/webparts",
                        replace: "static/js/webparts",
                        flags: "gi"
                    }
                ]
            }
        },

        // UGLIFY OPTIONS
        uglify: {
            head: {
                options: {
                    banner: "/*! <%= pkg.name %> <%= grunt.template.today(\"yyyy-mm-dd HHMM\") %> */\n",
                    sourceMap: "static/js/script.map",
                    sourceMapRoot: "/",
                    sourceMappingURL: "script.map"
                },
                files: {
                    'static/js/script.min.js': [
                        // Misc Plugins
                        "static/js/plugins/head/*.js"
                    ],
                }
            },
            deferred: {
                options: {
                    banner: "/*! <%= pkg.name %> <%= grunt.template.today(\"yyyy-mm-dd HHMM\") %> */\n",
                    sourceMap: "static/js/script.deferred.map",
                    sourceMapRoot: "/",
                    sourceMappingURL: "script.deferred.map"
                },
                files: {
                    'static/js/script.deferred.min.js': [
                        // Bootstrap
                        "static/js/bootstrap/**/*.js",

                        // Misc Plugins
                        "static/js/plugins/deferred/**/*.js",

                        // Deepend Modules
                        "../Deepend.WebParts.*/static/js/**/*.js",

                        // Generic Modules
                        "static/js/modules/**/*.js",

                        // The Strand Modules
                        "../VIN.WebParts.*/static/js/**/*.js",

                        // Main
                        "static/js/main.js"
                    ] // Save over the newly created script
                }
            },
        },

        // WATCH OPTIONS
        watch: {
            scripts: {
                files: ["**/*.js", "!**/node_modules/**", "!static/js/script.min.js", "!static/js/script.deferred.min.js"],
                tasks: ["uglify"] //Uglify
            },
            sass: {
                files: [
                    "**/*.scss",
                    "**/skins/*/*.scss",
                    "!**/node_modules/**"
                ],

                tasks: ['sass:dist', 'postcss']
            },
            replace: {
                files: ["**/*.map"],
                tasks: ["regex-replace"]
            }
        },

        //CSS + SASS
        //GRUNT SASS
        sass: {
            options: {
                sourceMap: true,
                outputStyle: 'compressed'
                //outputStyle: 'expanded'
            },
            dist: {
                files: {
                    'static/css/screen.css': 'static/css/src/screen.scss'
                }
            }
        },


        postcss: {
            options: {
                map: {
                  inline: false, // save all sourcemaps as separate files...
                  annotation: 'static/css/maps/' // ...to the specified directory
                },

                processors: [
                    require('pixrem')(), // add fallbacks for rem units

                    require('autoprefixer')({
                        //browsers: 'last 2 versions'
                        "browserslist": [
                            "iOS 7",
                            "iOS 8",
                            "last 2 versions"
                        ]
                    }), // add vendor prefixes

                    require('cssnano')() // minify the result
                ]
            },
            dist: {
                src: 'static/css/*.css'
            }
        },


    });

    // load our tasks
    grunt.loadNpmTasks("grunt-contrib-concat");
    grunt.loadNpmTasks("grunt-contrib-uglify");
    grunt.loadNpmTasks("grunt-contrib-watch");
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks("grunt-regex-replace");

    grunt.registerTask("default", ["watch"]);
    grunt.registerTask("scripts", ["uglify"]);
    grunt.registerTask('build', ['sass:dist', 'postcss', 'uglify']);
    grunt.registerTask('default', ['sass:dist', 'postcss', 'uglify', 'watch']);

    // TODO: Post uglify, replace the following
    // ../TheMarketPlace.WebParts.*?/static/js/webparts
    // static/js/webparts
}