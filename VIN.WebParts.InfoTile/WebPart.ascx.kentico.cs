// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.InfoTile
{
    using CMS.Helpers;

    using Deepend.Framework.Kentico.WebPart;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart, InfoTileModel>
    {
        #region "WebPart Property Names"

        /// <summary>
        /// Gets the pro p_ butto n_ text.
        /// </summary>
        public virtual string PROP_BUTTON_TEXT
        {
            get
            {
                return "ButtonText";
            }
        }

        /// <summary>
        /// Gets the pro p_ butto n_ url.
        /// </summary>
        public virtual string PROP_BUTTON_URL
        {
            get
            {
                return "ButtonUrl";
            }
        }

        /// <summary>
        /// Gets the pro p_ maplatlon g_ text.
        /// </summary>
        public virtual string PROP_MAPLATLONG_TEXT
        {
            get
            {
                return "MapLatLong";
            }
        }

        /// <summary>
        /// Gets the pro p_ contactaddres s_ text.
        /// </summary>
        public virtual string PROP_CONTACTADDRESS_TEXT
        {
            get
            {
                return "ContactAddress";
            }
        }

        /// <summary>
        /// Gets the pro p_ contactnumbe r_ text.
        /// </summary>
        public virtual string PROP_CONTACTNUMBER_TEXT
        {
            get
            {
                return "ContactNumber";
            }
        }

        public virtual string PROP_OpeningHours_TEXT
        {
            get
            {
                return "OpeningHours";
            }
        }

        public virtual string PROP_POBox_TEXT
        {
            get
            {
                return "POBox";
            }
        }

        public virtual string PROP_Email_TEXT
        {
            get
            {
                return "Email";
            }
        }

        public virtual string PROP_FaxNumber_TEXT
        {
            get
            {
                return "FaxNumber";
            }
        }

        #endregion

        #region "WebPart Default Values"

        /// <summary>
        /// Gets the defaul t_ butto n_ text.
        /// </summary>
        public virtual string DEFAULT_BUTTON_TEXT
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the defaul t_ butto n_ url.
        /// </summary>
        public virtual string DEFAULT_BUTTON_URL
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the defaul t_ maplatlon g_ text.
        /// </summary>
        public virtual string DEFAULT_MAPLATLONG_TEXT
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the defaul t_ contactaddres s_ text.
        /// </summary>
        public virtual string DEFAULT_CONTACTADDRESS_TEXT
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets the defaul t_ contactnumbe r_ text.
        /// </summary>
        public virtual string DEFAULT_CONTACTNUMBER_TEXT
        {
            get
            {
                return string.Empty;
            }
        }

        #endregion

        #region "WebPart Properties"

        /// <summary>
        /// Gets or sets the button text.
        /// </summary>
        public string ButtonText
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_BUTTON_TEXT),
                    this.DEFAULT_BUTTON_TEXT,
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_BUTTON_TEXT, value);
            }
        }

        /// <summary>
        /// Gets or sets the button url.
        /// </summary>
        public string ButtonUrl
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_BUTTON_URL),
                    this.DEFAULT_BUTTON_URL,
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_BUTTON_URL, value);
            }
        }

        /// <summary>
        /// Gets or sets the map lat long.
        /// </summary>
        public string MapLatLong
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_MAPLATLONG_TEXT),
                    this.DEFAULT_MAPLATLONG_TEXT,
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_MAPLATLONG_TEXT, value);
            }
        }

        /// <summary>
        /// Gets or sets the contact address.
        /// </summary>
        public string ContactAddress
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_CONTACTADDRESS_TEXT),
                    this.DEFAULT_CONTACTADDRESS_TEXT,
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_CONTACTADDRESS_TEXT, value);
            }
        }

        /// <summary>
        /// Gets or sets the contact number.
        /// </summary>
        public string ContactNumber
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_CONTACTNUMBER_TEXT),
                    this.DEFAULT_CONTACTNUMBER_TEXT,
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_CONTACTNUMBER_TEXT, value);
            }
        }

        public string OpeningHours
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_OpeningHours_TEXT),
                    string.Empty);
            }

            set
            {
                this.SetValue(this.PROP_OpeningHours_TEXT, value);
            }
        }

        public string POBox
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_POBox_TEXT),
                    string.Empty);
            }

            set
            {
                this.SetValue(this.PROP_POBox_TEXT, value);
            }
        }
        public string Email
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_Email_TEXT),
                    string.Empty);
            }

            set
            {
                this.SetValue(this.PROP_Email_TEXT, value);
            }
        }
        public string FaxNumber
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_FaxNumber_TEXT),
                    string.Empty);
            }

            set
            {
                this.SetValue(this.PROP_FaxNumber_TEXT, value);
            }
        }

        #endregion
    }
}