// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.InfoTile
{
    using System.Collections.Generic;

    using CMS.CustomTables;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Properties"

        /// <summary>
        /// The _open times.
        /// </summary>
        private Dictionary<string, string> _openTimes;

        /// <summary>
        /// Gets the open times.
        /// </summary>
        public Dictionary<string, string> OpenTimes
        {
            get
            {
                if (this._openTimes == null)
                {
                    var result = new Dictionary<string, string>();
                    var openTimes = string.Empty;

                    var daysOfWeek = new List<string>
                                         {
                                             "monday", 
                                             "tuesday", 
                                             "wednesday", 
                                             "thursday", 
                                             "friday", 
                                             "saturday", 
                                             "sunday"
                                         };

                    IEnumerable<CustomTableItem> openItems =
                        CustomTableItemProvider.GetItems("VIN.OpenTimes").WhereIn("Day", daysOfWeek);

                    foreach (var openItem in openItems)
                    {
                        var day = openItem.GetValue("Day", string.Empty);

                        if (!string.IsNullOrEmpty(day))
                        {
                            day = char.ToUpper(day[0]) + day.Substring(1);
                        }

                        result.Add(
                            day, 
                            openItem.GetValue("StartTime", string.Empty) + " - "
                            + openItem.GetValue("EndTime", string.Empty));
                    }

                    this._openTimes = result;
                }

                return this._openTimes;
            }
        }

        #endregion

        #region "Methods"

        #endregion
    }
}