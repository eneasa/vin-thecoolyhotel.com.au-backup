﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SubscribeController.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The signup controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.Modules.WebAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Web.Http;

    using Deepend.Framework.Kentico;

    using MailChimp;
    using MailChimp.Helper;
    using MailChimp.Lists;

    using VIN.Modules.WebAPI.Models;
    using CMS.EventLog;

    /// <summary>
    /// The signup controller.
    /// </summary>
    public class SubscribeController : ApiController
    {
        /// <summary>
        /// The post.
        /// </summary>
        /// <param name="Signup">
        /// The signup.
        /// </param>
        /// <returns>
        /// The <see cref="bool"/>.
        /// </returns>
        [HttpPost]
        [ActionName("Signup")]
        public bool Post([FromBody] SignupModel Signup)
        {
            var ApiKey = KenticoUtils.GetSetting("VIN.MailchimpApiKey");
            var ListId = KenticoUtils.GetSetting("VIN.MailchimpListId");
            var GroupingId = KenticoUtils.GetSetting("VIN.MailchimpGroupingId").ToInt32();

            try
            {
                EventLogProvider.LogInformation("Start", "Mailchimp", Signup.Email);
                var mergeVars = new MergeVar();

                if (GroupingId != null && !string.IsNullOrEmpty(Signup.Categories))
                {
                    var categories = Signup.Categories.Trim('|').Split('|');

                    mergeVars.Groupings = new List<Grouping>();
                    mergeVars.Groupings.Add(new Grouping());
                    mergeVars.Groupings[0].Id = GroupingId;
                    mergeVars.Groupings[0].GroupNames = new List<string>();

                    foreach (var category in categories)
                    {
                        mergeVars.Groupings[0].GroupNames.Add(category);
                    }
                }

                var mc = new MailChimpManager(ApiKey);

                var emailPar = new EmailParameter();
                emailPar.Email = Signup.Email;
                var results = mc.Subscribe(ListId, emailPar, mergeVars);

                return true;
            }
            catch (Exception ex)
            {
                EventLogProvider.LogException("MailChimp", "subscribe", ex);
                return false;
            }
        }
    }
}