﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="SignupModel.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The signup model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.Modules.WebAPI.Models
{
    /// <summary>
    /// The signup model.
    /// </summary>
    public class SignupModel
    {
        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the categories.
        /// </summary>
        public string Categories { get; set; }
    }
}