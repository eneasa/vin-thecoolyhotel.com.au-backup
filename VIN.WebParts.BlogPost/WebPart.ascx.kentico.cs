// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.BlogPost
{
    using CMS.Helpers;

    using Deepend.Framework.Kentico.WebPart;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart, BlogPostModel>
    {
        /// <summary>
        /// Gets the pro p_ pag e_ size.
        /// </summary>
        public virtual string PROP_PAGE_SIZE
        {
            get
            {
                return "PageSize";
            }
        }

        /// <summary>
        /// Gets the defaul t_ pag e_ size.
        /// </summary>
        public virtual int DEFAULT_PAGE_SIZE
        {
            get
            {
                return 9;
            }
        }

        /// <summary>
        /// Gets or sets the page size.
        /// </summary>
        public int PageSize
        {
            get
            {
                return ValidationHelper.GetInteger(
                    this.GetValue(this.PROP_PAGE_SIZE), 
                    this.DEFAULT_PAGE_SIZE, 
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_PAGE_SIZE, value);
            }
        }
    }
}