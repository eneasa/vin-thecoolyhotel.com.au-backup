﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="BlogEntyTreeItem.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The blog enty tree item.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.BlogPost.Models
{
    using System.Collections.Generic;

    /// <summary>
    /// The blog enty tree item.
    /// </summary>
    public class BlogEntyTreeItem
    {
        /// <summary>
        /// Gets or sets the text.
        /// </summary>
        public string Text { get; set; }

        /// <summary>
        /// Gets or sets the url.
        /// </summary>
        public string URL { get; set; }

        /// <summary>
        /// Gets or sets the parent node id.
        /// </summary>
        public int ParentNodeId { get; set; }

        /// <summary>
        /// Gets or sets the children.
        /// </summary>
        public List<BlogEntyTreeItem> Children { get; set; }
    }
}