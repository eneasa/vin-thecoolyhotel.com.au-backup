﻿+function($) {

    $.namespace("VIN", {
        InitBlogPostListing: function(settings) {
            console.log("VIN.WebParts.BlogPost.InitBlogPostListing", settings);

            this.each(function() {
                // Arrange
                var blogListingElem = $(this);
                var blogPostsWrapperElem = $(this).find(".posts").first();
                var blogPaginationElem = $(this).find(".pagination");
                var pageSize = blogPostsWrapperElem.data("page-size");
                blogPostsWrapperElem.data("current-page", 1);

                // Binding
                blogPaginationElem.on("click", ".page-item", function() {
                    blogPostsWrapperElem.data("current-page", $(this).data("page-index")); // Update current page number
                    updatePaginationDisplay(blogPaginationElem, blogPostsWrapperElem.data("current-page")); // Update pagination display
                    displayBlogsPage(blogPostsWrapperElem, blogPostsWrapperElem.data("current-page")); // Update display items
                });

                blogPaginationElem.on("click", ".arrow-item", function() {
                    var currentPage = blogPostsWrapperElem.data("current-page");
                    var targetPage = $(this).hasClass("left-arrow-item") ? currentPage - 1 : currentPage + 1;
                    blogPaginationElem.find(".page-item[data-page-index=\"" + targetPage + "\"]").trigger("click"); // Delegate to another event
                });

                // Init 
                blogPaginationElem.find(".page-item[data-page-index=\"" + blogPostsWrapperElem.data("current-page") + "\"]").trigger("click");
            });
        }
    });

    function displayBlogsPage(blogPostsWrapperElem, currentPage) {
        //blogPostsWrapperElem.hide(); // Hide posts wrapper
        //console.log('hide wrapper');

        blogPostsWrapperElem.find(".post-item").hide(); // Hide all blog post items
        blogPostsWrapperElem.find(".post-item[data-page-index=\"" + currentPage + "\"]").show(); // Show posts that are within the target page

        //blogPostsWrapperElem.show(); // Show posts wrapper
        //console.log('show wrapper');
    }

    function updatePaginationDisplay(paginationElem, currentPage) {
        paginationElem.find(".item").removeClass("active").removeClass("disabled"); // Reset markings
        paginationElem.find(".page-item[data-page-index=\"" + currentPage + "\"]").addClass("active"); // Set current page

        if (paginationElem.find(".page-item:first").hasClass("active")) { // Disable left arrow when at first page
            paginationElem.find(".left-arrow-item").addClass("disabled");
        }

        if (paginationElem.find(".page-item:last").hasClass("active")) { // Disable right arrow when at last page
            paginationElem.find(".right-arrow-item").addClass("disabled");
        }
    }

    $(document).ready(function() {
        $(".blog-listing").TheMarketPlace().InitBlogPostListing();
    });

}(jq111);