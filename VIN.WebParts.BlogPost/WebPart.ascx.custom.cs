// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.BlogPost
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.UI.HtmlControls;

    using CMS.DocumentEngine;
    using CMS.PortalEngine;

    using Deepend.Framework.Kentico.Modules.ImageCrop;

    using TheCoolyHotel.Domain.Models;

    using VIN.WebParts.BlogPost.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        /// <summary>
        /// The _drop down items.
        /// </summary>
        private List<BlogEntyTreeItem> _dropDownItems;

        /// <summary>
        /// The _items.
        /// </summary>
        private List<BlogPostModel> _items;

        /// <summary>
        /// The _paged items.
        /// </summary>
        private List<BlogPostModel> _pagedItems;

        /// <summary>
        /// The _tags.
        /// </summary>
        private List<string> _tags;

        /// <summary>
        /// Gets a value indicating whether edit mode.
        /// </summary>
        public bool EditMode
        {
            get
            {
                return this.PageManager.ViewMode == ViewModeEnum.Edit;
            }
        }

        /// <summary>
        /// Gets the inventory root.
        /// </summary>
        public override string InventoryRoot
        {
            get
            {
                return this.CurrentDocument.NodeAliasPath;
            }
        }

        /// <summary>
        /// Gets the tag value.
        /// </summary>
        public string TagValue
        {
            get
            {
                var tag = HttpContext.Current.Request.Params["tag"];

                return (!string.IsNullOrEmpty(tag)) ? HttpUtility.UrlDecode(tag) : string.Empty;
            }
        }

        /// <summary>
        /// Gets the category value.
        /// </summary>
        public string CategoryValue
        {
            get
            {
                var category = HttpContext.Current.Request.Params["category"];

                return (!string.IsNullOrEmpty(category)) ? HttpUtility.UrlDecode(category) : string.Empty;
            }
        }

        /// <summary>
        /// Gets the items.
        /// </summary>
        public override List<BlogPostModel> Items
        {
            get
            {
                if (this._items == null)
                {
                    var InventoryRoot = (!string.IsNullOrEmpty(this.TagValue))
                                            ? this.CurrentDocument.Parent.NodeAliasPath
                                            : this.CurrentDocument.NodeAliasPath;

                    var result =
                        DocumentHelper.GetDocuments(BlogPostModel.KENTICO_CLASSNAME)
                            .Path(InventoryRoot, PathTypeEnum.Section)
                            .Published()
                            .Culture(this.CurrentDocument.DocumentCulture)
                            .TypedResult.Items.ToDomainModels<BlogPostModel>()
                            .OrderBy(x => x.BlogPostDate)
                            .ToList();

                    if (!string.IsNullOrEmpty(this.TagValue))
                    {
                        result =
                            result.Where(
                                x =>
                                x.DocumentTags != null && x.DocumentTags.ToLower().Contains(this.TagValue.ToLower()))
                                .ToList();
                    }

                    if (!string.IsNullOrEmpty(this.CategoryValue))
                    {
                        result =
                            result.Where(
                                x =>
                                x.DocumentTags != null
                                && x.DocumentTags.ToLower().Contains(this.CategoryValue.ToLower())).ToList();
                    }

                    this._items = result;
                }

                return this._items ?? new List<BlogPostModel>();
            }
        }

        /// <summary>
        /// Gets the paged items.
        /// </summary>
        public List<BlogPostModel> PagedItems
        {
            get
            {
                if (this._pagedItems == null && this.Items != null && this.Items.Count > 0)
                {
                    this._pagedItems = this.Items.OrderByDescending(x => x.BlogPostDate).ToList();
                }

                return this._pagedItems ?? new List<BlogPostModel>();
            }
        }

        /// <summary>
        /// Gets the tags.
        /// </summary>
        public List<string> Tags
        {
            get
            {
                if (this._tags == null)
                {
                    var InventoryRoot = (!string.IsNullOrEmpty(this.TagValue))
                                            ? this.CurrentDocument.Parent.NodeAliasPath
                                            : this.CurrentDocument.NodeAliasPath;

                    var result =
                        DocumentHelper.GetDocuments(BlogPostModel.KENTICO_CLASSNAME)
                            .Path(InventoryRoot, PathTypeEnum.Section)
                            .Published()
                            .Culture(this.CurrentDocument.DocumentCulture)
                            .TypedResult.Items.ToDomainModels<BlogPostModel>()
                            .OrderBy(x => x.BlogPostDate)
                            .ToList();

                    var tags = new List<string>();

                    foreach (var blogPostModel in result)
                    {
                        if (blogPostModel.DocumentTags != null)
                        {
                            var tag =
                                blogPostModel.DocumentTags.Replace(@"""", string.Empty)
                                    .Replace(@"\", string.Empty)
                                    .Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                            tags.AddRange(tag.Select(s => s.Trim()));
                        }
                    }

                    this._tags = tags.Distinct().ToList();
                }

                return this._tags ?? new List<string>();
            }
        }

        /// <summary>
        /// Gets the drop down items.
        /// </summary>
        public List<BlogEntyTreeItem> DropDownItems
        {
            get
            {
                if (this._dropDownItems == null)
                {
                    var result =
                        DocumentHelper.GetDocuments(BlogPostModel.KENTICO_CLASSNAME)
                            .Published()
                            .Culture(this.CurrentDocument.DocumentCulture)
                            .TypedResult.Items.ToDomainModels<BlogPostModel>()
                            .OrderBy(x => x.BlogPostDate)
                            .ToList();

                    var list =
                        result.GroupBy(g => g.BlogPostDate.Year)
                            .Select(
                                g =>
                                new BlogEntyTreeItem
                                    {
                                        Text = g.Key.ToString(), 
                                        Children =
                                            g.GroupBy(g1 => g1.BlogPostDate.ToString("MMMM"))
                                            .Select(
                                                g1 =>
                                                new BlogEntyTreeItem
                                                    {
                                                        Text = g1.Key, 
                                                        Children =
                                                            g1.Select(
                                                                i =>
                                                                new BlogEntyTreeItem
                                                                    {
                                                                        Text
                                                                            =
                                                                            i
                                                                            .BlogPostTitle, 
                                                                        URL
                                                                            =
                                                                            i
                                                                            .AbsoluteURL, 
                                                                        ParentNodeId
                                                                            =
                                                                            i
                                                                            .NodeParentID
                                                                    })
                                                            .ToList()
                                                    })
                                            .ToList()
                                    })
                            .ToList();

                    foreach (var blogEntyTreeItem in list)
                    {
                        foreach (var entyTreeItem in blogEntyTreeItem.Children)
                        {
                            var parentNode = entyTreeItem.Children.Select(i => i.ParentNodeId).FirstOrDefault();
                            var tp = new TreeProvider();
                            var parent = tp.SelectSingleNode(parentNode);
                            entyTreeItem.URL = parent.RelativeURL;
                        }
                    }

                    this._dropDownItems = list;
                }

                return this._dropDownItems ?? new List<BlogEntyTreeItem>();
            }
        }

        /// <summary>
        /// The on load.
        /// </summary>
        /// <param name="e">
        /// The e.
        /// </param>
        protected override void OnLoad(EventArgs e)
        {
            try
            {
                base.OnLoad(e);

                if (this.Item != null)
                {
                    var headMain = (HtmlHead)this.Page.Master.FindControl("mainHead");

                    if (!string.IsNullOrWhiteSpace(this.Item.DocumentName))
                    {
                        var htMetaTitle = new HtmlMeta();
                        htMetaTitle.Attributes.Add("name", "og:title");
                        htMetaTitle.Attributes.Add(
                            "content", 
                            this.Item.DocumentName.Trim() + " - The Coolangatta Hotel");
                        headMain.Controls.Add(htMetaTitle);
                    }

                    if (this.Item.ImageGuid != null && !this.Item.ImageGuid.Equals(Guid.Empty))
                    {
                        var htMetaImg = new HtmlMeta();
                        htMetaImg.Attributes.Add("name", "og:image");
                        htMetaImg.Attributes.Add("content", ImageCropUtils.GetImageUrl(this.Item.ImageGuid, "300x300"));
                        headMain.Controls.Add(htMetaImg);
                    }
                }
            }
            catch
            {
            }
        }
    }
}