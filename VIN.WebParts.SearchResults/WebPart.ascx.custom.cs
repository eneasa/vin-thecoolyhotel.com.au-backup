// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.SearchResults
{
    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Properties"

        #endregion

        #region "Methods"

        #endregion
    }
}