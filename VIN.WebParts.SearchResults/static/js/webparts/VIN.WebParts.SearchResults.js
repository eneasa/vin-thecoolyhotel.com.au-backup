﻿+function($) {

    $(document).ready(function() {

        // Search Module
        if ($.find(".module.site-search").length > 0) {
            var searchInput = $(".module.site-search").find(".gsc-input");
            var searchTermDisplay = $(".module.site-search").find(".search-term");
            var searchParam = location.search.split("q=")[1] ? location.search.split("q=")[1] : "";

            if (searchParam.split("&")[0]) {
                searchParam = searchParam.split("&")[0];
            }

            searchTermDisplay.text(unescape(searchParam));
        }

    });

}(jq111);