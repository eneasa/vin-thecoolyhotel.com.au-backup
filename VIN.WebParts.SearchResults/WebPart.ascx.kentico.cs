// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.SearchResults
{
    using Deepend.Framework.Kentico.WebPart;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart>
    {
        // WebPartBase<WebPart, DomainType>
        #region "WebPart Default Values"

        #endregion

        #region "WebPart Properties"

        #endregion
    }
}