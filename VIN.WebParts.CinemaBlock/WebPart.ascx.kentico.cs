// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.CinemaBlock
{
    using CMS.Helpers;

    using Deepend.Framework.Kentico.WebPart;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart>
    {
        #region "WebPart Property Names"

        /// <summary>
        /// Gets the pro p_ externa l_ pag e_ url.
        /// </summary>
        public virtual string PROP_EXTERNAL_PAGE_URL
        {
            get
            {
                return "ExternalPageUrl";
            }
        }

        /// <summary>
        /// Gets the pro p_ enabl e_ script.
        /// </summary>
        public virtual string PROP_ENABLE_SCRIPT
        {
            get
            {
                return "EnableScript";
            }
        }

        #endregion

        #region "WebPart Default Values"

        /// <summary>
        /// Gets the defaul t_ externa l_ pag e_ url.
        /// </summary>
        public virtual string DEFAULT_EXTERNAL_PAGE_URL
        {
            get
            {
                return string.Empty;
            }
        }

        /// <summary>
        /// Gets a value indicating whether defaul t_ enabl e_ script.
        /// </summary>
        public virtual bool DEFAULT_ENABLE_SCRIPT
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region "WebPart Properties"

        /// <summary>
        /// Gets or sets the external page url.
        /// </summary>
        public string ExternalPageUrl
        {
            get
            {
                return ValidationHelper.GetString(
                    this.GetValue(this.PROP_EXTERNAL_PAGE_URL), 
                    this.DEFAULT_EXTERNAL_PAGE_URL, 
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_EXTERNAL_PAGE_URL, value);
            }
        }

        /// <summary>
        /// Gets or sets a value indicating whether enable script.
        /// </summary>
        public bool EnableScript
        {
            get
            {
                return ValidationHelper.GetBoolean(
                    this.GetValue(this.PROP_ENABLE_SCRIPT), 
                    this.DEFAULT_ENABLE_SCRIPT, 
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_ENABLE_SCRIPT, value);
            }
        }

        #endregion
    }
}