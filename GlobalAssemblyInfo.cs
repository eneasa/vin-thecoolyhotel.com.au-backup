﻿// --------------------------------------------------------------------------------------------------------------------
// <copyright file="GlobalAssemblyInfo.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   GlobalAssemblyInfo.cs
// </summary>
// --------------------------------------------------------------------------------------------------------------------

using System.Reflection;

[assembly: AssemblyCompany("Deepend Group")]
[assembly: AssemblyProduct("Coolangatta Hotel")]
[assembly: AssemblyCopyright("Copyright © Deepend Group 2017")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyVersion("8.0.*")]
[assembly: AssemblyInformationalVersion("8.0.0")]