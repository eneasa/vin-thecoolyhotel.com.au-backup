// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.custom.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.FeatureTile
{
    using System;

    using CMS.PortalEngine;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart
    {
        #region "Constants"

        /// <summary>
        /// Gets a value indicating whether edit mode.
        /// </summary>
        public bool EditMode
        {
            get
            {
                return this.PageManager.ViewMode == ViewModeEnum.Edit;
            }
        }

        #endregion

        #region "Properties"

        /// <summary>
        /// Gets the item.
        /// </summary>
        public override FeatureTileModel Item
        {
            get
            {
                try
                {
                    return base.Item;
                }
                catch (Exception ex)
                {
                    return null;
                }
            }
        }

        #endregion
    }
}