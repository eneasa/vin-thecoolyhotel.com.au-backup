// --------------------------------------------------------------------------------------------------------------------
// <copyright file="WebPart.ascx.kentico.cs" company="Deepend">
//   Copyright (c) Deepend. All rights reserved.
// </copyright>
// <summary>
//   The web part.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace VIN.WebParts.FeatureTile
{
    using CMS.Helpers;

    using Deepend.Framework.Kentico.WebPart;

    using TheCoolyHotel.Domain.Models;

    /// <summary>
    /// The web part.
    /// </summary>
    public partial class WebPart : WebPartBase<WebPart, FeatureTileModel>
    {
        #region "WebPart Property Names"

        /// <summary>
        /// Gets the pro p_ shareable.
        /// </summary>
        public virtual string PROP_SHAREABLE
        {
            get
            {
                return "Shareable";
            }
        }

        #endregion

        #region "WebPart Default Values"

        /// <summary>
        /// Gets a value indicating whether defaul t_ shareable.
        /// </summary>
        public virtual bool DEFAULT_SHAREABLE
        {
            get
            {
                return false;
            }
        }

        #endregion

        #region "WebPart Properties"

        /// <summary>
        /// Gets or sets a value indicating whether shareable.
        /// </summary>
        public bool Shareable
        {
            get
            {
                return ValidationHelper.GetBoolean(
                    this.GetValue(this.PROP_SHAREABLE), 
                    this.DEFAULT_SHAREABLE, 
                    this.CultureInfo);
            }

            set
            {
                this.SetValue(this.PROP_SHAREABLE, value);
            }
        }

        #endregion
    }
}